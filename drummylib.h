/**
  @file
  drummylib (DFL) - Drummyfish's small C99 public domain (CC0) header-only
                    utility library in KISS/suckless style for writing
                    KISS/suckless programs.

  The library is written in pure C99 for maximum portability and can work even
  without the standard C library (with the exception of <stdint.h>). If you want
  standard library to be used to allow for specific features, define #DFL_STDLIB
  before including this library. If this is the case, only these standard
  functions will be used by default:

  - TODO

  The library is expected to be used even on very simple computers, such as
  embedded, and so tries to be eficient, somewhat in both CPU cycles and memory,
  and eliminate a lot of dependencies for the program.

  This doesn't try to be the best and most flexible solution to all issues but
  rather a reasonably simple and efficient solution to many common problems,
  plus an alternative to the standard library. It is also meant as a copy-paste
  repository of code, so if you need a very specific customization, simply copy
  the code and rewrite it as you wish, or hack this in any other way you find
  fit.

  This library is supposed to be self-contained and self-documenting and have as
  few dependencies as possible, so e.g. floats/doubles are not used unless
  absolutely necessary, considering some computers lack a HW floating point
  unit. Integer alternatives for float fnctions are used wherever possible:
  these will use integer numbers normalized by DFL_INT_SCALE, effectively
  implementing a fixed point arithmetic. 32bit signed int (int32_t) is the
  preferred  data type of these functions, but other data types may be used
  depending on the situation.

  Many functions will have mutiple alternatives to choose from, such as
  different data types or macro implementation. Keep in mind macros are supposed
  to be used for computing values at compile time, so use them in that way.
  No doubles are used -- if you want doubles instead of floats, run
  's/float/double' in vim.

  Many things are controlled by macros and can be redefined for porting,
  optimization and flexibility. See CONTROL MACROS section.

  author: Miloslav "drummyfish" Ciz
  license: CC0 1.0, public domain
  version: 0.1

  TODO:
  - create drummytool, a CLI program that uses this lib and does all kinds of
    stuff like text and image processing, games etc.
  - INDICATE ERRORS (conversions etc.) IN GLOBAL VAR?!?!?!
  - general:
    - bit operations: reverse, swap
    - macro to supressed unused variable warning
    - macro for branch optimization (likely)
    - overflow check?
    - functions: toBase/fromBase, converts numbers, e.g. toBase(5,2) = 101
    - interpolations, extrapolations
    - 2_TO(x) macro
    - checksum
    - fibonnaci
    - change endianness
    - approximate divisions possibly faster than accurate ones, e.g. x / 3 ~= 
      (3 * x) / 8
  - CLI parameters parsing
  - TUI
  - bit arrays
  - A* and other AI (state search, backtracking, ...)
  - roman numerals
  - platform dependent functions calling Bash commands, e.g. for listing files etc.
  - morse code
  - float:
    - operation emulation (for compiler independency)
    - analyzer (extract mantisa, exponent, get precision etc.)
  - maze generation
  - complex numbers
  - normal array operations
    - average, median, ...
  - musical:
    - bytebeat
    - note frequencies
    - procedural composition
    - compatible with midi and wav
  - randomness:
    - random functions
    - random distributions (conversions)
    - hashes
    - getting random numbers from /dev/random?
    - noises: perlin etc.
    - randomness test (mine)
  - time functions:
    - unix timestamp to/from time
    - date arithmetics
    - get time (using stdlib)
  - arbitrary precision, platform independent int numbers:
    precision set with macro
  - constants
  - naturaly language processors? (esperanto, english, ...)
  - languages:
    - brainfuck
    - custom simple scripting lang + bytecode
  - string math expression evaluator and convertor to post/prefix
  - compression:
    - general simplpe dict-based ASCII text compression
    - huffman coding
  - error correction codes?
  - unicode approximator by ASCII
  - benchmark functions:
    - a macro that runs specified command N times and measures the average time
  - url parser
  - unit conversions
  - math:
    - sin, cos, pow, sqrt, log, factorial, ...
    - linear algebra
    - permutation ordering
    - numeric:
      - integration
      - differention
      - find root
    - number theory:
      - primes: is prime, get nth prime
      - fibonacci
      - factorization
      - various weird number types and sequences
    - pi/e/etc. digit calculation
  - sorts:
    make a macro template to allow custom types
  - graph algorithms:
    - shortest path
    - travelling salesman
  - simple 3D raycasting?
  - encryption, signing etc., including simple symmetric cyphers
  - extended ASCII
  - evolutional algorithms?
  - formats:
    - ppm
    - farbfeld
    - obj
    - csv
    - key: value
    - midi
    - wav
    - gif? bmp?
    - base64
    - partial support for complex formats, e.g. get resolution, FPS, duration
      from video format etc.
  - QR codes
  - steganography
  - checksums
  - fractals:
    - mandelbrot
  - simple neural nets?
  - turing machine simulation:
    will interpret a mini language like this:
      byte 0: 1 bit (output) + command for the state 0 with input 0
      byte 1: number of next state from state 0 with input 0 
              (so maximum of states is 256)
      byte 2: 1 bit (output) + command for the state 0 with input 1
      byte 3: number of next state from state 0 with input 1
      byte 4: command for the state 1 with input 0
      ...
    possible commands (up to 128 = 1 byte - 1 bit) can be:
      - move right on tape ('>')    \
      - move left on tape  ('<')     > basic commands
      - do nothing         (' ')    /
      - move by 2 right             \
      - move by 2 left               \
        ...                           | extended commands, optional
      - move forward before next 0
      - move forward before next 1
      - move backward before previous 0
        ...
      - move to random cell
        ...

  - simple evolutional machine learning:
    this will try to learn a specified transformation (at least partially) of
    a bit array of length N (max 128 bits) to an output array of bits of
    length M (max 128 bits). The result of learning is an algorithm for a
    turing machine specified above. The learning will be evolutional, using a
    user specified fitness function that will assign any algorithm a fitness
    value (e.g. by testing the algorithm on images from a learning data set).
  - shape collision checking?
  - simple quantum computer simulator?
  - string functions:
    if possible return pointer to processed string to allow chaining of funcs
    - reverse
    - toLower/toUpper
    - translate (replace according chars)
    - sort
    - trim
    - compare
    - edit distance
    - lorem ipsum generator
    - numbers to/from strings
    - text formatting, e.g. line wrapping
    - stats: wordcount, char histogram etc.
    - filenames (join, extension etc.)
  - json, xml, html:
    - function: get item from format string by string selector
  - regexps
  - debugging:
    - hexdump raw memory
    - pretty print arrays etc.
    - logging
    - profiling support?
  - games:
    add AI where possible
    - chess
    - checkers
    - tetris (name block game because of trademark?)
    - snake
    - sudoku: solving, generation
    - minesweeper
    - backgammon
    - go
    - some card games
    - 2048
  - graphics:
    - line algorithm (DDA)
    - small custom built-in font?
    - bitmap to ASCII art
    - other shapes: circle, ellipse, triangle, ...
    - colors space conversions: RGB, HSL, HSV, ...
    - palettes: convert to/from, compute palette
    - operations (darken, ...) on different pixel formats (565, RGB, BGR, ...)
    - pixelart scaling
    - FXAA filter
    - dithering
    - convolution and other filters
    - image warping
    - stuff detection in pictures
  - Fourier transforms, FFT
  - 2D signed distance field (computing SDF, SDF font/shape rendering, ...)
  - DCT
  - cellular automata:
    - game of life
    - rock paper scissors
    - elementary (rule X)
  - langton's ant
*/

#ifndef DRUMMYLIB_H
#define DRUMMYLIB_H

#include <stdint.h>
  
// CONTROL MACROS =============================================================

/*
  Macros used for control, porting, optimization, flexibility etc. They have
  default behavior, but can be redefined by you before including this library.
  These macros are:

  DFL_LOG(str)                    optional lib. logging, taking const char *,
                                  can be useful for debugging

  DFL_PUT_CHAR(c)                 put single char to stdout, for printing funcs
  DFL_GET_CHAR                    get a single char from stdin, for reading
  DFL_FLUSH                       flush stdout

  DFL_FILE_OPEN_READ(filename)    open file by filename globally for reading
  DFL_FILE_OPEN_WRITE(filename)   open file by filename globally for writing
  DFL_FILE_OK                     1/0 value indicating if file was opened
  DFL_FILE_WRITE(byte)            write byte value to globally opened file
  DFL_FILE_READ(byte)             read byte from glob. opened file into variable
  DFL_FILE_EOF                    1/0 value indicating if EOF was reached
  DFL_FILE_CLOSE                  close globally opened file
*/

#ifndef DFL_LOG
  #define DFL_LOG(str) ;
#endif

// error handling:

  #define DFL_ERROR_NONE 0     ///< success value
  #define DFL_ERROR 1          ///< error without further specification

  #ifndef DFL_SET_ERROR
    /** The library uses this macro to set error and success values. By default
        it sets the global DFL_error variable, but you may redefine it to e.g.
        call and exception handling function (remember that this macro called
        with 0 means no error). */

    uint8_t DFL_error = 0;

    #define DFL_SET_ERROR(n,s) { DFL_error = (n); if (n != DFL_ERROR_NONE) DFL_LOG("ERROR: " s); }
  #endif

  #define DFL_SET_SUCCESS DFL_SET_ERROR(DFL_ERROR_NONE,"");

/**
  This number represents the equivalent of floating point 1.0, but for integer
  (fixed point) arithmetic. Functions that avoid using floating point will use
  integers and normalize numbers by DFL_INT_SCALE. This number should be a power
  of two for efficiency. Too low value can result in inaccuracies, too high in
  overflows.
*/
#ifndef DFL_INT_SCALE
  #define DFL_INT_SCALE 1024
#endif

#ifdef DFL_STDLIB
  #include <stdlib.h>
  #include <stdio.h>

  FILE *DFL_file = 0;
  uint8_t DFL_fileByte = 0;
  uint8_t DFL_eof = 0;

  #ifndef DFL_FILE_OPEN_READ
    #define DFL_FILE_OPEN_READ(f) \
      { DFL_file = fopen(f,"rb"); \
        if (!DFL_file) DFL_LOG("WARNING: couldn't open file for reading"); }
  #endif

  #ifndef DFL_FILE_OPEN_WRITE
    #define DFL_FILE_OPEN_WRITE(f) \
      { DFL_file = fopen(f,"wb"); \
        if (!DFL_file) DFL_LOG("WARNING: couldn't open file for writing"); }
  #endif

  #ifndef DFL_FILE_OK
    #define DFL_FILE_OK (DFL_file != 0)
  #endif

  #ifndef DFL_FILE_WRITE
    #define DFL_FILE_WRITE(b) \
      { DFL_fileByte = b; fwrite(&DFL_fileByte,1,1,DFL_file); }
  #endif

  #ifndef DFL_FILE_READ
    #define DFL_FILE_READ(b) \
      { DFL_eof = fread(&DFL_fileByte,1,1,DFL_file) == 0; b = DFL_fileByte; }
  #endif

  #ifndef DFL_EOF
    #define DFL_EOF DFL_eof
  #endif

  #ifndef DFL_FILE_CLOSE
    #define DFL_FILE_CLOSE { fclose(DFL_file); DFL_file = 0; }
  #endif

  #ifndef DFL_PUT_CHAR
    #define DFL_PUT_CHAR(c) putchar(c)
  #endif

  #ifndef DFL_GET_CHAR
    #define DFL_GET_CHAR getchar()
  #endif

  #ifndef DFL_FLUSH
    #define DFL_FLUSH fflush(stdout)
  #endif

#else // no stdlib
  #ifndef DFL_FILE_OPEN_READ
    #define DFL_FILE_OPEN_READ(f) \
      DFL_LOG("DFL_FILE_OPEN_READ not defined, doing nothing.")
  #endif

  #ifndef DFL_FILE_OPEN_WRITE
    #define DFL_FILE_OPEN_WRITE(f) \
      DFL_LOG("DFL_FILE_OPEN_WRITE not defined, doing nothing.")
  #endif

  #ifndef DFL_FILE_WRITE
    #define DFL_FILE_WRITE(b) ;
  #endif

  #ifndef DFL_EOF
    #define DFL_EOF 1
  #endif

  #ifndef DFL_FILE_OK
    #define DFL_FILE_OK 1
  #endif

  #ifndef DFL_FILE_READ
    #define DFL_FILE_READ(b) ;
  #endif

  #ifndef DFL_FILE_CLOSE
    #define DFL_FILE_CLOSE ;
  #endif

  #ifndef DFL_PUT_CHAR
    #define DFL_PUT_CHAR(c) ;
  #endif

  #ifndef DFL_GET_CHAR
    #define DFL_GET_CHAR(c) 0
  #endif

  #ifndef DFL_FLUSH
    #define DFL_FLUSH ;
  #endif
#endif // DFL_STDLIB

/* general ---------------------------------------------------------------------

  General helpers. */

  typedef int8_t DFL_Bool;

  const char DFL_emptyString[1] = "";

  #ifndef DFL_NULL
    #define DFL_NULL ((void *) 0)
  #endif

  #define DFL_TRUE 1
  #define DFL_FALSE 0

  #define DFL_WRAP(x,limit) \
    ((((x) >= 0) ? (x) : (((limit) + (x) % (limit)))) % (limit))

  #define DFL_MAX_BASE 36

  static inline int32_t DFL_wrapI32(int32_t x, uint32_t limit);
  static inline int32_t DFL_wrapF(float x, float limit);

  /**
    Copies memory by bytes, dst must point to a memory that has at least size
    bytes allocated. If any of the pointers is DFL_NULL, nothing happens.

    @return pointer identical to "dst"
  */
  uint8_t *DFL_memoryCopy(const uint8_t *src, uint16_t size, uint8_t *dst);

  /**
    Sets memory bytes to given value. the pointer is DFL_NULL, nothing happens.

    @return pointer ideantical to "memory"
  */
  uint8_t *DFL_memorySet(uint8_t *memory, uint16_t size, uint8_t value);

  /**
    Compares two memory areas byte by byte.  If any of the pointers is DFL_NULL,
    nothing happens.
  */
  DFL_Bool DFL_memoryCompare(const uint8_t *memory, const uint8_t *memory2, uint16_t size);

  /**
    Tests the system on whether it is big or little endian.
  */
  DFL_Bool DFL_systemIsLittleEndian(void);

  /**
    Gets the address width of the system, i.e. the size of a memory pointer in
    bytes.
  */
  #define DFL_SYSTEM_ADDRESS_WIDTH (sizeof(char *))

  /**
    For a number says how many significang digits it has in given base
    ('-' is not counted as a digit).
  */
  uint8_t DFL_numberDigits(int64_t number, uint16_t base);
 
  /**
    For a number of given width and signedness computes the maximum number of 
    digits it can take in given base ('-' is not counted as a digit).
  */
  uint8_t DFL_maxDigits(uint8_t byteWidth, uint16_t base, DFL_Bool signedType);

  /// For preventing division by zero
  #define DFL_NONZERO(x) (((x) != 0) ? (x) : 1)

  static inline int32_t DFL_nonzeroI32(int32_t x);
  static inline int32_t DFL_nonzeroF(float x);

  #define DFL_MIN(a,b) ((a) < (b) ? (a) : (b))
  #define DFL_MAX(a,b) ((a) < (b) ? (b) : (a))

  static inline int32_t DFL_minI32(int32_t a, int32_t b);
  static inline int32_t DFL_maxI32(int32_t a, int32_t b);

  static inline int32_t DFL_minF(float a, float b);
  static inline int32_t DFL_maxF(float a, float b);

  #define DFL_SIGN(x) ((x) > 0 ? 1 : ((x) < 0 ? -1 : 0))

  static inline int8_t  DFL_signI8(int8_t x);
  static inline int32_t DFL_signI32(int32_t x);
  static inline int32_t DFL_signF(float x);

  #define DFL_2_TO(n) (0x00000001 << n)

  #define DFL_UNUSED(var) (void)(var) ///< For suppressing unused var. warnings.

  #define DFL_LIKELY(cond) __builtin_expect(!!(cond), 1) 
  #define DFL_UNLIKELY(cond) __builtin_expect(!!(cond), 0) 

  #define DFL_ROMAN_MAX 3999 ///< Maximum value representable in Roman numerals.

  #ifndef DFL_NEWLINE
    #define DFL_NEWLINE '\n'
  #endif

  #ifndef DFL_RADIX_SEPARATOR
    #define DFL_RADIX_SEPARATOR '.'
  #endif
 
  #define DFL_MOD_F(f,m) (((float) (f)) - ((int64_t) (((float) (f)) / ((float) (m)))) * ((float) (m)))

  float DFL_modF(float f, float m);

  #define DFL_CAST(x,type) ((type) (x))

  static inline DFL_Bool DFL_floatsEqual(float a, float b, float tolerance);

  char DFL_digitSymbol(uint8_t digit);
  int8_t DFL_symbolDigit(char symbol);

  uint32_t DFL_toBase(uint16_t number, uint8_t base);

  static inline uint16_t DFL_romanDigitValue(char digit);

  #define DFL_BIT_ARRAY_SIZE(length) (length == 0 ? 0 : ((((int32_t) length) - 1) / 8 + 1))

  static inline uint8_t DFL_bitArrayGet(uint8_t *array, uint32_t index);
  static inline uint8_t DFL_bitArraySet(uint8_t *array, uint32_t index, uint8_t value); 

/* command line parameters -----------------------------------------------------
  
  Simple CLI argument parsing. Supported argument formats are:

  -cvalue    arg starts with '-' followed by a single char c and value without
             space, e.g.: -imyFile.txt
  value      single value, e.g.: myFile.txt

  The "p" parameter in parsing functions specifies either the char c for the
  former format (e.g. 'i' will look for the 1st argument starting with "-i")
  or the sequential number (when < 32) of the argument for the latter format
  (e.g. 1 will look for the 2nd argument that doesn't start with '-'). */

  const char *DFL_CLIArgGetStr(
    char p, const char *defaultVal, uint8_t argc, const char **argv);

  uint8_t DFL_CLIArgGet0to9(
    char p, uint8_t defaultVal, uint8_t argc, const char **argv);

  int32_t DFL_CLIArgGetI32(
    char p, int32_t defaultVal, uint8_t argc, const char **argv);
 
  // TODO: float

  DFL_Bool DFL_CLIArgPresent(char p, uint8_t argc, const char **argv);

/* prints ----------------------------------------------------------------------
  
  In order for prints to work either standard library has to be allowed
  (define DFL_STDLIB) or define custom DFL_PUT_CHAR function. Prints are mostly
  just convenience wrappers around functions that stream serialized data types
  to stdout/stderr/files, so see these functions for more flexibile printing. */

  void DFL_printLn(void);
  void DFL_printS(const char *string);
  void DFL_printC(char c);
  void DFL_printI(int32_t number);
  void DFL_printIHex(int32_t number);
  void DFL_printIBin(int32_t number);
  void DFL_printF(float number);
  void DFL_printRoman(uint16_t number);
  void DFL_printLnC(char c);
  void DFL_printLnI(int32_t number);
  void DFL_printLnS(const char *string);
  void DFL_printLnRoman(uint16_t number);

/* files ------------------------------------------------------------------------

  Functions related to files, work only if standard library is allowed
  (define DFL_STDLIB) or custom file functions are defined. */

  /**
    Writes binary data to file.
  */
  DFL_Bool DFL_fileWrite(const uint8_t *data, uint32_t size,
    const char *filename);

  /**
    Reads binary data from file.
    @return Number of bytes read, 0 meaning the file couldn't be opened.
  */
  uint32_t DFL_fileRead(uint8_t *data, uint32_t maxSize, const char *filename);

  /**
    Reads a file as ASCII text into string and terminates it with 0.
    @return Number of bytes read, 0 meaning the file couldn't be opened.
  */
  uint32_t DFL_fileReadString(char *s, uint32_t maxSize, const char *filename);

/* math ------------------------------------------------------------------------

  Mathematical functions. */

  #define DFL_0_2PI(x) \
    (((x) >= 0) ? DFL_MOD_F(x,2 * DFL_PI_F) : \
    (2 * DFL_PI_F + DFL_MOD_F(x,2 * DFL_PI_F)))

  #define DFL_SIN_0_2PI_F(rad) /* Bhaskara I's approximation formula */ \
    (16 * (rad) * (DFL_PI_F - (rad))) / \
    (5 * DFL_PI_F * DFL_PI_F - 4 * (rad) * (DFL_PI_F - (rad)))

  /**
    Macro for computing sine at compile time, intended to be used with
    constants only!
  */
  #define DFL_SIN_F(rad) \
    (DFL_0_2PI(rad) <= DFL_PI_F) ? \
      DFL_SIN_0_2PI_F(DFL_0_2PI(rad)) : \
      (-1 * DFL_SIN_0_2PI_F(DFL_0_2PI(rad) - DFL_PI_F))
 
  #define DFL_COS_F(rad) DFL_SIN_F((rad) + DFL_PI_F / 2)

  uint32_t DFL_factorial32(uint32_t n);
  uint64_t DFL_factorial64(uint64_t n);
  static inline DFL_Bool DFL_isEven(uint32_t n);

/* strings/chars/text ----------------------------------------------------------
  
  These are functions working exclusively on strings, usually in place. Other
  functions working with strings can be found under streams section. */

  /**
    Initializes string with spaces (' ') and terminates is with 0. Useful in
    combination with streaming functions.
  */
  static inline char *DFL_stringInit(char *string, uint32_t size);

  static inline DFL_Bool DFL_charIsWhite(char c);
  static inline DFL_Bool DFL_charIsDigit(char c);
  static inline DFL_Bool DFL_charIsPrintable(char c);

  static inline char DFL_charToUpper(char c);
  static inline char DFL_charToLower(char c);

  uint32_t DFL_stringLength(const char *s);

  char *DFL_stringToUpper(char *s);
  char *DFL_stringToLower(char *s);

  char *DFL_stringReverse(char *s);

  DFL_Bool DFL_stringEquals(const char *s1, const char *s2);

  char *DFL_stringTrimL(char *s);

  /** Gets an ASCII char of which given unicode char is a homoglyph (similarly
    looking glyph), or the char itself. This is not a definitive version of the
    algorithm and doesn't always work correctly. */
  uint32_t DFL_getUnicodeHomoglyph(uint32_t wChar);

  /// Approximates a unicode character with ASCII character. 
  char DFL_unicodeToAscii(uint32_t wChar);

/* streams ---------------------------------------------------------------------

  These are implementations of various text streams (stdout, string, file, ...)
  to which data can be serialized (see the serialization section). 

  Functions streaming to strings return the pointer to the next character after
  their output in the string, allowing to use the functions like this:

  DFL_streamEndToStr(
    DFL_streamBToStr(
      DFL_streamAToStro(
        ...(DFL_stringInit(string,size))...)))
  */

  /// Function used to feed a single character into a specific type of stream.
  typedef void (*DFL_StreamWriteFunc)(char);  

  /// Function used to get a single character from a specific type of stream.
  typedef char (*DFL_StreamReadFunc)(void);  

  char *DFL_strStreamOut = 0;       ///< Current input string stream pointer.
  const char *DFL_strStreamIn = 0;  ///< Current output stream pointer.

  // stream types:

    void DFL_streamToOut(char c);
    char DFL_streamFromIn(void);

    void DFL_streamToStr(char c);  ///< Writes to global DFL_strStreamOut.
    char DFL_streamFromStr(void);  ///< Reads from global DFL_strStreamIn.

    void DFL_streamToFile(char c); ///< Writes to globally open write file.
    char DFL_streamFromFile(void); ///< Reads from globally open read file.

    void DFL_streamToNull(char c); ///< Does nothing, mostly for testing.

    // TODO: file, err

  // streaming functions:

    void        DFL_streamStrToOut(char *s);
    char       *DFL_streamStrToStr(char *dst, const char *s);
    void        DFL_streamStrToFile(char *s);

    void        DFL_streamI32ToOut(int32_t n, uint8_t base, uint8_t zeroPad);
    int32_t     DFL_streamI32FromIn(uint8_t base);
    char       *DFL_streamI32ToStr(char *dst, int32_t n, uint8_t base, uint8_t zeroPad);
    const char *DFL_streamI32FromStr(const char *stream, uint8_t base, int32_t *result);
    void        DFL_streamI32ToFile(int32_t n, uint8_t base, uint8_t zeroPad);

    void        DFL_streamU32ToOut(uint32_t n, uint8_t base, uint8_t zeroPad);
    char       *DFL_streamU32ToStr(char *dst, uint32_t n, uint8_t base, uint8_t zeroPad);

    void        DFL_streamU64ToOut(uint64_t n, uint8_t base, uint8_t zeroPad);
    char       *DFL_streamU64ToStr(char *dst, uint64_t n, uint8_t base, uint8_t zeroPad);
   
    void        DFL_streamI64ToOut(int64_t n, uint8_t base, uint8_t zeroPad);
    char       *DFL_streamI64ToStr(char *dst, int64_t n, uint8_t base, uint8_t zeroPad);

    void        DFL_streamFToOut(float f, uint8_t base, uint8_t maxDecimals);
    float       DFL_streamFFromIn(uint8_t base);
    char       *DFL_streamFToStr(char *dst, float f, uint8_t base, uint8_t maxDecimals);
    const char *DFL_streamFFromStr(const char *stream, uint8_t base, float *result);

    char       *DFL_streamBitArrayToStr(char *dst, uint8_t *array, uint32_t length);
 
    /// Streams a terminating 0 to given string.
    char       *DFL_streamEndToStr(char *dst);

    void        DFL_streamRomanToOut(uint16_t n);
    uint16_t    DFL_streamRomanFromIn(void);
    char       *DFL_streamRomanToStr(char *dst, uint16_t n);
    const char *DFL_streamRomanFromStr(const char *stream, uint16_t *result);

    const char *DFL_streamUtf8CharFromStr(const char *stream, uint32_t *result);
    void        DFL_streamUtf8CharToFile(uint32_t wChar);
    void        DFL_streamUtf8CharToOut(uint32_t wChar);

/* TODO ------------------------------------------------------------------------
  */

  /**
    Performs a single step of a brainfuck program.

    @return DFL_FALSE if the program has terminated, DFL_TRUE otherwise
  */

  DFL_Bool DFL_brainfuckStep(const char *program, char *tape, uint16_t tapeSize, 
    uint16_t *programPos, uint16_t *tapePos, DFL_StreamWriteFunc fOut,
    DFL_StreamReadFunc fIn);
 
  /**
    Executes a brainfuck program.

    @return number of total steps executed
  */
  uint32_t DFL_brainfuckRun(const char *program, char *tape, uint16_t tapeSize,
    DFL_StreamWriteFunc fOut, DFL_StreamReadFunc fIn); 

  // chess:

  #define DFL_CHESS_STATE_SIZE 70

  /**
    Represents chess state as a string in this format:
    - First 64 characters represent the chess board (A8, B8, ... H1), each field
      can be either a piece (prnbkqPRNBKQ) or empty ('.'). I.e. the stat state
      looks like this:

              RNBQKBNR
              PPPPPPPP
              ........
              ........
              ........
              ........
              pppppppp
              rnbqkbnr

      And the square numbering is this:

              0  1  2  3  4  5  6  7
              8  9  10 11 12 13 14 15
              16 17 18 19 20 21 22 23
              24 25 26 27 28 29 30 31
              32 33 34 35 36 37 38 39
              40 41 42 43 44 45 46 47
              48 49 50 51 52 53 54 55
              56 57 58 59 60 61 62 63

    - After this a few more bytes follow to represent some global states. These
      bytes are:
      - 64: En-passant state for white, each bit says whether the pawn at the
        corresponding column has moved two squares forward in the previous move,
        i.e. whether it is possible to en-passant capture it.
      - 65: En-passant state for black, same as previous but for black pawns.
      - 66: Special byte holding various states:
        - bit 0 (least significant bit): Whether white can short castle, i.e.
          1 means all the condition for white short castling are met, 0
          otherwise.
        - bit 1: Whether white can long castle.
        - bit 2: Whether black can short castle.
        - bit 3: Whether black can long castle.
        - Remaining bits are always set to 1 (to not allow the byte to
          become 0).
      - 67, 68: 16 bit unsigned number saying the number of plys (half-moves)
        that have already been played, also determining whose turn it currently
        is.
      - 69: The last byte is always 0 to properly terminate the string in case
        someone tries to print it.
    - The state is designed so as to be simple and also print-friendly, i.e. you
      can simply print it with line break after 8 characters to get a human
      readable representation of the board.
  */
  typedef char DFL_ChessState[DFL_CHESS_STATE_SIZE];

  /**
    Maximum number of moves a chess piece can have (a queen in the middle of the
    board).
  */
  #define DFL_CHESS_PIECE_MAX_MOVES 25

  #define DFL_CHESS_START_STATE \
    {82, 78, 66, 81, 75, 66, 78, 82,\
     80, 80, 80, 80, 80, 80, 80, 80,\
     46, 46, 46, 46, 46, 46, 46, 46,\
     46, 46, 46, 46, 46, 46, 46, 46,\
     46, 46, 46, 46, 46, 46, 46, 46,\
     46, 46, 46, 46, 46, 46, 46, 46,\
     112,112,112,112,112,112,112,112,\
     114,110,98, 113,107,98, 110,114,\
     0,0,(char) DFL_11110000,0,0,0}

  /**
    A set of possible moves (board quare indices) for a single chess piece. This
    array is always terminated by the value 255.
  */
  typedef uint8_t DFL_ChessMoveSet[DFL_CHESS_PIECE_MAX_MOVES + 1];

  static inline DFL_Bool DFL_chessPieceIsWhite(char piece); 
  static inline DFL_Bool DFL_chessSquareIsWhite(uint8_t square);
  void DFL_chessGetPieceMoves(const DFL_ChessState state, uint8_t pieceSquare, DFL_ChessMoveSet result); 

/* serialization --------------------------------------------------------------

  These function serialize various data typed to an abstract stream of
  characters (represented by a stream function). Many serialization functions
  return the number of serialized chars. There are also deserialization
  functions that do the opposite. */

  // TODO: instead of void let ser. return the num of serialized chars

  uint32_t DFL_charsSerialized = 0;       ///< counter of serialized characters

  uint32_t DFL_serializeStr(DFL_StreamWriteFunc streamFunc, const char *s);

  uint32_t DFL_serializeI32(DFL_StreamWriteFunc streamFunc, int32_t n, uint8_t base, uint8_t zeroPad);
  uint32_t DFL_serializeU32(DFL_StreamWriteFunc streamFunc, uint32_t n, uint8_t base, uint8_t zeroPad);
  uint32_t DFL_serializeI64(DFL_StreamWriteFunc streamFunc, int64_t n, uint8_t base, uint8_t zeroPad);
  uint32_t DFL_serializeU64(DFL_StreamWriteFunc streamFunc, uint64_t n, uint8_t base, uint8_t zeroPad);
  uint32_t DFL_serializeF(DFL_StreamWriteFunc streamFunc, float f, uint8_t base, uint8_t maxDecimals);

  uint32_t DFL_serializeBitArray(DFL_StreamWriteFunc streamFunc, uint8_t *bitArray, uint32_t length);

  int32_t  DFL_deserializeI32(DFL_StreamReadFunc streamFunc, uint8_t base);
  float    DFL_deserializeF(DFL_StreamReadFunc streamFunc, uint8_t base);

  /// Serializes Roman numeral.
  void     DFL_serializeRoman(DFL_StreamWriteFunc streamFunc, uint16_t n);
  uint16_t DFL_deserializeRoman(DFL_StreamReadFunc streamFunc);

  uint32_t DFL_deserializeUtf8Char(DFL_StreamReadFunc streamFunc);
  void     DFL_serializeUtf8Char(DFL_StreamWriteFunc streamFunc, uint32_t wChar);

  uint32_t DFL_serializeChessState(DFL_StreamWriteFunc streamFunc, const DFL_ChessState state, const DFL_ChessState *moves);


  /**
    Serializes part of memory in human readable form, also known as hexdump.
  */
  const uint8_t *DFL_serializeMemoryDump(DFL_StreamWriteFunc streamFunc,
                                       const uint8_t *memory, uint8_t stepWidth,
                                       uint16_t steps, uint8_t columns,
                                       uint8_t base, DFL_Bool signedType,
                                       uint8_t addrBase);

// GENERAL ====================================================================

/* Constants, some in float/double format. If integer/fixed format is needed,
   simply use these in a literal to convert to desired format. */

#define DFL_PI_F           3.1415926535897932384626433832795028841971
#define DFL_E_F            2.7182818284590452353602874713526624977572
#define DFL_GOLDEN_RATIO_F 1.6180339887498948482045868343656381177203
#define DFL_SQRT2_F        1.4142135623730950488016887242096980785696
#define DFL_SQRT10_F       3.1622776601683793319988935444327185337195
#define DFL_GOOGOL_F       1.0E100

#define DFL_I8_MAX 127
#define DFL_U8_MAX 255

#define DFL_I16_MAX 32767 
#define DFL_U16_MAX 65535

#define DFL_I24_MAX 16777215
#define DFL_U24_MAX 8388607

#define DFL_I32_MAX 2147483647
#define DFL_U32_MAX 4294967295

#define DFL_I64_MAX 9223372036854775806
#define DFL_U64_MAX 18446744073709551615

/**
  Macro for binary expressions. Only put in positive integer decimal LITERALS
  that contain only 1s and 0s and aren't longer than 32 places.
*/
#define DFL_B(val)\
  (\
    (val % 10) +\
    ((val / 10) % 10) * 2 +\
    ((val / 100) % 10) * 4 +\
    ((val / 1000) % 10) * 8 +\
    ((val / 10000) % 10) * 16 +\
    ((val / 100000) % 10) * 32 +\
    ((val / 1000000) % 10) * 64 +\
    ((val / 10000000) % 10) * 128 +\
    ((val / 100000000) % 10) * 256 +\
    ((val / 1000000000) % 10) * 512 +\
    (((val / 1000000000) / 10) % 10) * 1024 +\
    (((val / 1000000000) / 100) % 10) * 2048 +\
    (((val / 1000000000) / 1000) % 10) * 4096 +\
    (((val / 1000000000) / 10000) % 10) * 8192 +\
    (((val / 1000000000) / 100000) % 10) * 16384 +\
    (((val / 1000000000) / 1000000) % 10) * 32768 +\
    (((val / 1000000000) / 10000000) % 10) * 65536 +\
    (((val / 1000000000) / 100000000) % 10) * 131072 +\
    (((val / 1000000000) / 1000000000) % 10) * 262144 +\
    ((((val / 1000000000) / 1000000000) / 10) % 10) * 524288 +\
    ((((val / 1000000000) / 1000000000) / 100) % 10) * 1048576 +\
    ((((val / 1000000000) / 1000000000) / 1000) % 10) * 2097152 +\
    ((((val / 1000000000) / 1000000000) / 10000) % 10) * 4194304 +\
    ((((val / 1000000000) / 1000000000) / 100000) % 10) * 8388608 +\
    ((((val / 1000000000) / 1000000000) / 1000000) % 10) * 16777216 +\
    ((((val / 1000000000) / 1000000000) / 10000000) % 10) * 33554432 +\
    ((((val / 1000000000) / 1000000000) / 100000000) % 10) * 67108864 +\
    ((((val / 1000000000) / 1000000000) / 1000000000) % 10) * 134217728 +\
    (((((val / 1000000000) / 1000000000) / 1000000000) / 10) % 10) * 268435456 +\
    (((((val / 1000000000) / 1000000000) / 1000000000) / 100) % 10) * 536870912 +\
    (((((val / 1000000000) / 1000000000) / 1000000000) / 1000) % 10) * 1073741824 +\
    (((((val / 1000000000) / 1000000000) / 1000000000) / 10000) % 10) * 2147483648\
  )

/*
  Byte binary literals (0b...). Standard C99 doesn't have these so they require
  compiler extensions. These constants here can be used if you want to not rely
  on the extensions.
*/

#define DFL_00000000 0x00
#define DFL_00000001 0x01
#define DFL_00000010 0x02
#define DFL_00000011 0x03
#define DFL_00000100 0x04
#define DFL_00000101 0x05
#define DFL_00000110 0x06
#define DFL_00000111 0x07
#define DFL_00001000 0x08
#define DFL_00001001 0x09
#define DFL_00001010 0x0a
#define DFL_00001011 0x0b
#define DFL_00001100 0x0c
#define DFL_00001101 0x0d
#define DFL_00001110 0x0e
#define DFL_00001111 0x0f
#define DFL_00010000 0x10
#define DFL_00010001 0x11
#define DFL_00010010 0x12
#define DFL_00010011 0x13
#define DFL_00010100 0x14
#define DFL_00010101 0x15
#define DFL_00010110 0x16
#define DFL_00010111 0x17
#define DFL_00011000 0x18
#define DFL_00011001 0x19
#define DFL_00011010 0x1a
#define DFL_00011011 0x1b
#define DFL_00011100 0x1c
#define DFL_00011101 0x1d
#define DFL_00011110 0x1e
#define DFL_00011111 0x1f
#define DFL_00100000 0x20
#define DFL_00100001 0x21
#define DFL_00100010 0x22
#define DFL_00100011 0x23
#define DFL_00100100 0x24
#define DFL_00100101 0x25
#define DFL_00100110 0x26
#define DFL_00100111 0x27
#define DFL_00101000 0x28
#define DFL_00101001 0x29
#define DFL_00101010 0x2a
#define DFL_00101011 0x2b
#define DFL_00101100 0x2c
#define DFL_00101101 0x2d
#define DFL_00101110 0x2e
#define DFL_00101111 0x2f
#define DFL_00110000 0x30
#define DFL_00110001 0x31
#define DFL_00110010 0x32
#define DFL_00110011 0x33
#define DFL_00110100 0x34
#define DFL_00110101 0x35
#define DFL_00110110 0x36
#define DFL_00110111 0x37
#define DFL_00111000 0x38
#define DFL_00111001 0x39
#define DFL_00111010 0x3a
#define DFL_00111011 0x3b
#define DFL_00111100 0x3c
#define DFL_00111101 0x3d
#define DFL_00111110 0x3e
#define DFL_00111111 0x3f
#define DFL_01000000 0x40
#define DFL_01000001 0x41
#define DFL_01000010 0x42
#define DFL_01000011 0x43
#define DFL_01000100 0x44
#define DFL_01000101 0x45
#define DFL_01000110 0x46
#define DFL_01000111 0x47
#define DFL_01001000 0x48
#define DFL_01001001 0x49
#define DFL_01001010 0x4a
#define DFL_01001011 0x4b
#define DFL_01001100 0x4c
#define DFL_01001101 0x4d
#define DFL_01001110 0x4e
#define DFL_01001111 0x4f
#define DFL_01010000 0x50
#define DFL_01010001 0x51
#define DFL_01010010 0x52
#define DFL_01010011 0x53
#define DFL_01010100 0x54
#define DFL_01010101 0x55
#define DFL_01010110 0x56
#define DFL_01010111 0x57
#define DFL_01011000 0x58
#define DFL_01011001 0x59
#define DFL_01011010 0x5a
#define DFL_01011011 0x5b
#define DFL_01011100 0x5c
#define DFL_01011101 0x5d
#define DFL_01011110 0x5e
#define DFL_01011111 0x5f
#define DFL_01100000 0x60
#define DFL_01100001 0x61
#define DFL_01100010 0x62
#define DFL_01100011 0x63
#define DFL_01100100 0x64
#define DFL_01100101 0x65
#define DFL_01100110 0x66
#define DFL_01100111 0x67
#define DFL_01101000 0x68
#define DFL_01101001 0x69
#define DFL_01101010 0x6a
#define DFL_01101011 0x6b
#define DFL_01101100 0x6c
#define DFL_01101101 0x6d
#define DFL_01101110 0x6e
#define DFL_01101111 0x6f
#define DFL_01110000 0x70
#define DFL_01110001 0x71
#define DFL_01110010 0x72
#define DFL_01110011 0x73
#define DFL_01110100 0x74
#define DFL_01110101 0x75
#define DFL_01110110 0x76
#define DFL_01110111 0x77
#define DFL_01111000 0x78
#define DFL_01111001 0x79
#define DFL_01111010 0x7a
#define DFL_01111011 0x7b
#define DFL_01111100 0x7c
#define DFL_01111101 0x7d
#define DFL_01111110 0x7e
#define DFL_01111111 0x7f
#define DFL_10000000 0x80
#define DFL_10000001 0x81
#define DFL_10000010 0x82
#define DFL_10000011 0x83
#define DFL_10000100 0x84
#define DFL_10000101 0x85
#define DFL_10000110 0x86
#define DFL_10000111 0x87
#define DFL_10001000 0x88
#define DFL_10001001 0x89
#define DFL_10001010 0x8a
#define DFL_10001011 0x8b
#define DFL_10001100 0x8c
#define DFL_10001101 0x8d
#define DFL_10001110 0x8e
#define DFL_10001111 0x8f
#define DFL_10010000 0x90
#define DFL_10010001 0x91
#define DFL_10010010 0x92
#define DFL_10010011 0x93
#define DFL_10010100 0x94
#define DFL_10010101 0x95
#define DFL_10010110 0x96
#define DFL_10010111 0x97
#define DFL_10011000 0x98
#define DFL_10011001 0x99
#define DFL_10011010 0x9a
#define DFL_10011011 0x9b
#define DFL_10011100 0x9c
#define DFL_10011101 0x9d
#define DFL_10011110 0x9e
#define DFL_10011111 0x9f
#define DFL_10100000 0xa0
#define DFL_10100001 0xa1
#define DFL_10100010 0xa2
#define DFL_10100011 0xa3
#define DFL_10100100 0xa4
#define DFL_10100101 0xa5
#define DFL_10100110 0xa6
#define DFL_10100111 0xa7
#define DFL_10101000 0xa8
#define DFL_10101001 0xa9
#define DFL_10101010 0xaa
#define DFL_10101011 0xab
#define DFL_10101100 0xac
#define DFL_10101101 0xad
#define DFL_10101110 0xae
#define DFL_10101111 0xaf
#define DFL_10110000 0xb0
#define DFL_10110001 0xb1
#define DFL_10110010 0xb2
#define DFL_10110011 0xb3
#define DFL_10110100 0xb4
#define DFL_10110101 0xb5
#define DFL_10110110 0xb6
#define DFL_10110111 0xb7
#define DFL_10111000 0xb8
#define DFL_10111001 0xb9
#define DFL_10111010 0xba
#define DFL_10111011 0xbb
#define DFL_10111100 0xbc
#define DFL_10111101 0xbd
#define DFL_10111110 0xbe
#define DFL_10111111 0xbf
#define DFL_11000000 0xc0
#define DFL_11000001 0xc1
#define DFL_11000010 0xc2
#define DFL_11000011 0xc3
#define DFL_11000100 0xc4
#define DFL_11000101 0xc5
#define DFL_11000110 0xc6
#define DFL_11000111 0xc7
#define DFL_11001000 0xc8
#define DFL_11001001 0xc9
#define DFL_11001010 0xca
#define DFL_11001011 0xcb
#define DFL_11001100 0xcc
#define DFL_11001101 0xcd
#define DFL_11001110 0xce
#define DFL_11001111 0xcf
#define DFL_11010000 0xd0
#define DFL_11010001 0xd1
#define DFL_11010010 0xd2
#define DFL_11010011 0xd3
#define DFL_11010100 0xd4
#define DFL_11010101 0xd5
#define DFL_11010110 0xd6
#define DFL_11010111 0xd7
#define DFL_11011000 0xd8
#define DFL_11011001 0xd9
#define DFL_11011010 0xda
#define DFL_11011011 0xdb
#define DFL_11011100 0xdc
#define DFL_11011101 0xdd
#define DFL_11011110 0xde
#define DFL_11011111 0xdf
#define DFL_11100000 0xe0
#define DFL_11100001 0xe1
#define DFL_11100010 0xe2
#define DFL_11100011 0xe3
#define DFL_11100100 0xe4
#define DFL_11100101 0xe5
#define DFL_11100110 0xe6
#define DFL_11100111 0xe7
#define DFL_11101000 0xe8
#define DFL_11101001 0xe9
#define DFL_11101010 0xea
#define DFL_11101011 0xeb
#define DFL_11101100 0xec
#define DFL_11101101 0xed
#define DFL_11101110 0xee
#define DFL_11101111 0xef
#define DFL_11110000 0xf0
#define DFL_11110001 0xf1
#define DFL_11110010 0xf2
#define DFL_11110011 0xf3
#define DFL_11110100 0xf4
#define DFL_11110101 0xf5
#define DFL_11110110 0xf6
#define DFL_11110111 0xf7
#define DFL_11111000 0xf8
#define DFL_11111001 0xf9
#define DFL_11111010 0xfa
#define DFL_11111011 0xfb
#define DFL_11111100 0xfc
#define DFL_11111101 0xfd
#define DFL_11111110 0xfe
#define DFL_11111111 0xff

// STDIO ======================================================================

DFL_Bool DFL_fileWrite(const uint8_t *data, uint32_t size, const char *filename)
{
  DFL_LOG("saving file");
  DFL_LOG(filename);

  DFL_FILE_OPEN_WRITE(filename);

  if (!DFL_FILE_OK)
    return DFL_FALSE;

  for (uint32_t i = 0; i < size; ++i)
  {
    DFL_FILE_WRITE(*data);
    data++;
  }

  DFL_FILE_CLOSE;

  DFL_LOG("file saved");

  DFL_SET_SUCCESS
  return DFL_TRUE;
}

uint32_t DFL_fileRead(uint8_t *data, uint32_t maxSize, const char *filename)
{
  DFL_LOG("reading file");

  DFL_FILE_OPEN_READ(filename);

  if (!DFL_FILE_OK)
    return 0;

  uint32_t bytesRead = 0;

  while (1)
  {
    DFL_FILE_READ(*data)
    data++;

    if (DFL_EOF || (bytesRead >= maxSize))
      break;

    bytesRead++;
  }

  DFL_LOG("file read");

  DFL_FILE_CLOSE;

  DFL_SET_SUCCESS
  return bytesRead;
}

uint32_t DFL_fileReadString(char *s, uint32_t maxSize, const char *filename)
{
  uint32_t bytesRead = DFL_fileRead((uint8_t *) s,maxSize,filename);
  s[DFL_MIN(bytesRead,maxSize - 1)] = 0;
  return bytesRead;
}

uint32_t DFL_factorial32(uint32_t n)
{
  return n == 0 ? 1 : (n * DFL_factorial32(n - 1));
}

uint64_t DFL_factorial64(uint64_t n)
{
  return n == 0 ? 1 : (n * DFL_factorial64(n - 1));
}

DFL_Bool DFL_isEven(uint32_t n)
{
  return (n % 2) == 0;
}

uint32_t DFL_stringLength(const char *s)
{
  uint32_t length = 0;

  while (*s != 0)
  {
    s++;
    length++;
  }

  return length;
}

DFL_Bool DFL_stringEquals(const char *s1, const char *s2)
{
  while (*s1 == *s2)
  {
    if (*s1 == 0)
      return DFL_TRUE;

    s1++;
    s2++;
  }

  return DFL_FALSE;
}

char *DFL_stringReverse(char *s)
{
  char *p1, *p2;

  p1 = s;
  p2 = s;

  while (*p2 != 0)
    p2++;

  p2--;

  while (p2 > p1)
  {
    char tmp = *p1;
    *p1 = *p2;
    *p2 = tmp;
    p1++;
    p2--;
  }

  return s;
}

void DFL_printLn(void)
{
  DFL_PUT_CHAR(DFL_NEWLINE);
}

void DFL_printS(const char *string)
{
  while (*string != 0)
  {
    DFL_PUT_CHAR(*string);
    string++;
  }

  DFL_FLUSH;
}

void DFL_printC(char c)
{
  DFL_PUT_CHAR(c);
}
  
void DFL_printI(int32_t number)
{
  DFL_streamI32ToOut(number,10,1);
}
  
void DFL_printF(float number)
{
  DFL_streamFToOut(number,10,6);
}

void DFL_printIHex(int32_t number)
{
  DFL_printS("0x");
  DFL_streamI32ToOut(number,16,1);
}

void DFL_printIBin(int32_t number)
{
  DFL_printS("0b");
  DFL_streamI32ToOut(number,2,1);
}

void DFL_printLnC(char c)
{
  DFL_printC(c);
  DFL_printLn();
}

void DFL_printLnI(int32_t number)
{
  DFL_printI(number);
  DFL_printLn();
}

void DFL_printLnS(const char *string)
{
  DFL_printS(string);
  DFL_printLn();
}

void DFL_printRoman(uint16_t number)
{
  DFL_streamRomanToOut(number);
}

void DFL_printLnRoman(uint16_t number)
{
  DFL_printRoman(number);
  DFL_printLn();
}
 
DFL_Bool DFL_charIsWhite(char c)
{
  return c == ' ' || c == DFL_NEWLINE || c == '\t';
}
  
DFL_Bool DFL_charIsPrintable(char c)
{
  return c >= 32;
}

DFL_Bool DFL_charIsDigit(char c)
{
  return c >= '0' && c <= '9';
}
 
char *DFL_stringTrimL(char *s)
{
  char *p1 = s;
  char *p2 = s;

  while (DFL_charIsWhite(*p2))
    p2++; 

  while (*p2 != 0)
  {
    *p1 = *p2;
    p1++;
    p2++;
  }

  *p1 = 0;

  return s;
}

char DFL_digitSymbol(uint8_t number)
{
  return (number < 10 ? '0' : ('a' - 10)) + number;
}
  
int8_t DFL_symbolDigit(char symbol)
{
  return
    (symbol >= '0' ?
      (symbol <= '9' ?
        (symbol - '0') :
        (symbol >= 'A' ?
          (symbol <= 'Z' ?
            (symbol - 'A' + 10) :
            (symbol >= 'a' ?
              (symbol <= 'z' ?
                (symbol - 'a' + 10) :
                -1) :
              -1
            )
          ) :
          -1
        )
      ) :
      -1);
}

char *DFL_stringInit(char *string, uint32_t size)
{
  char *result = string;

  for (uint32_t i = 0; i < (size - 1); ++i)
  {
    *string = ' ';
    string++;
  }

  *string = 0;

  return result;
}

uint32_t DFL_serializeStr(DFL_StreamWriteFunc streamFunc, const char *s)
{
  DFL_charsSerialized = 0;

  while (*s != 0)
  {
    streamFunc(*s);
    s++;
  }

  return DFL_charsSerialized;
}

float DFL_deserializeF(DFL_StreamReadFunc streamFunc, uint8_t base)
{
  if ((base <= 1) || (base > DFL_MAX_BASE))
  {
    DFL_SET_ERROR(DFL_ERROR,"float deserialization illegal base");
    return 0.0;
  }

  float result = 0.0;
  char c;
  float sign = 1.0;

  do
  {
    c = streamFunc();
  } while (DFL_charIsWhite(c));

  if (c == '-')
  {
    sign = -1.0;
    c = streamFunc();
  }
 
  while (1) // whole part
  {
    if (c == DFL_RADIX_SEPARATOR)
    {
      c = streamFunc();
      break;
    }

    int8_t digit = DFL_symbolDigit(c);

    if ((digit < 0) || (digit >= base))
      break;

    result = (result * base) + digit;

    c = streamFunc();
  } 

  float fraction = 1.0 / base;

  while (1) // fraction part
  {
    int8_t digit = DFL_symbolDigit(c);

    if ((digit < 0) || (digit >= base))
      break;

    result += digit * fraction;

    fraction /= base;

    c = streamFunc();
  }

  if ((c == 'e') || (c == 'E')) // potential exponential part
  {
    int32_t order = 0;
    int8_t orderPositive = 1;

    c = streamFunc();

    if (c == '-')
    {
      orderPositive = 0;
      c = streamFunc();
    }

    while (1)
    {
      int8_t digit = DFL_symbolDigit(c);
      
      if ((digit < 0) || (digit >= base))
        break;
      
      order = order * base + digit;
    
      c = streamFunc();
    }

    if (orderPositive)
    {
      for (; order > 0; order--)
        result *= base;
    }
    else
    {
      for (; order > 0; order--)
        result /= base;
    }
  }
  
  DFL_SET_SUCCESS  
  return result * sign;
}

uint32_t DFL_serializeF(DFL_StreamWriteFunc streamFunc, float f, uint8_t base, uint8_t maxDecimals)
{
  DFL_charsSerialized = 0;

  if ((base <= 1) || (base > DFL_MAX_BASE))
  {
    DFL_SET_ERROR(DFL_ERROR,"float serialization illegal base");
    return 0;
  }

  if (f < 0.0)
  {
    streamFunc('-');
    f *= -1.0;
  }

  char b[32];
  char *buff = b;

  int8_t length = 0;

  float radixPart = DFL_MOD_F(f,1.0);

  while (((f >= 1.0) || (length < 1)) && (length < 32))
  {
    float digit = DFL_MOD_F(f,base);

    if ((digit >= base) || (digit < 0))
      digit = 0;
 
    *buff = DFL_digitSymbol(digit);

    buff++;

    f /= base;

    length++;
  }

  while (length > 0)
  {
    length--;
    buff--;
    streamFunc(*buff);
  }

  streamFunc(DFL_RADIX_SEPARATOR);

  length = 0;

  if (maxDecimals == 0)
    maxDecimals = 255;

  while ((radixPart > 0.0 || length < 1) && length < maxDecimals)
  {
    radixPart *= base;
    streamFunc(DFL_digitSymbol(radixPart));
    radixPart = DFL_MOD_F(radixPart,1.0);
    length++;
  }

  DFL_SET_SUCCESS
  return DFL_charsSerialized;
}

uint32_t DFL_serializeBitArray(DFL_StreamWriteFunc streamFunc, uint8_t *bitArray, uint32_t length)
{
  DFL_charsSerialized = 0;

  bitArray--;

  uint8_t pos = 0;

  uint8_t b;

  while (length != 0)
  {
    if ((pos % 8) == 0)
    {
      bitArray++;
      b = *bitArray;
    }
   
    streamFunc('0' + (b & 0x01));
    b >>= 1;

    length--;
    pos++;
  }

  return DFL_charsSerialized;
}

uint32_t DFL_serializeChessState(DFL_StreamWriteFunc streamFunc, const DFL_ChessState state, const DFL_ChessState *moves)
{
  DFL_charsSerialized = 0;

  const char *square = state;

  for (uint8_t i = 0; i < 64; ++i)
  {
    if ((i % 8 == 0) && (i != 0))
      streamFunc(DFL_NEWLINE);

    char c = *square;

    char squareColor = DFL_chessSquareIsWhite(i) ? ':' : '.';

    streamFunc(squareColor);

    streamFunc(c == '.' ? squareColor : *square);

    square++;
  }

  return DFL_charsSerialized;
}

DFL_Bool DFL_systemIsLittleEndian(void)
{
  uint8_t test[] = {1, 0};

  uint16_t number = *((uint16_t *) test);

  return number == 1;
}

const uint8_t *DFL_serializeMemoryDump(DFL_StreamWriteFunc streamFunc,
                                       const uint8_t *memory, uint8_t stepWidth,
                                       uint16_t steps, uint8_t columns,
                                       uint8_t base, DFL_Bool signedType,
                                       uint8_t addrBase)
{
  if ((memory == DFL_NULL) || (stepWidth == 0) || (stepWidth > 4) ||
      (columns == 0) || (base > DFL_MAX_BASE) || (addrBase == 0) ||
      (addrBase > DFL_MAX_BASE))
  {
    DFL_SET_ERROR(DFL_ERROR,"memory dump wrong parameter");
    return memory;
  }

  if ((stepWidth == 3) && !signedType)
  {
    DFL_SET_ERROR(DFL_ERROR,"memory dump can only take unsigned type for width 3");
    return memory;
  }

  const uint8_t *result = memory;

  uint8_t column = columns;
  int32_t number;

  uint8_t pad = DFL_maxDigits(stepWidth,base,signedType);

  char ascii[columns + 1];
  char *asciiPos = ascii;
  ascii[columns] = 0;
    
  #define as(t) ((t) (*((t *) memory)))

  while (1)
  {
    if (column == 0) // ascii
    {
      column = columns;

      streamFunc(' ');
      streamFunc('|');

      DFL_serializeStr(streamFunc,ascii);
      asciiPos = ascii;

      streamFunc('|');

      streamFunc(DFL_NEWLINE);
    }

    if (steps == 0)
      break;

    if (column == columns) // address
    {
      // TODO: address align

      if (DFL_SYSTEM_ADDRESS_WIDTH == 8) // #if macro doesn't work
        DFL_serializeU64(streamFunc,(uint64_t) memory,addrBase,0);
      else
        DFL_serializeU32(streamFunc,(uint32_t) memory,addrBase,0);

      streamFunc('|');
      streamFunc(' ');
      streamFunc(' ');
    }  

    number = 0;

    switch (stepWidth)
    {
      case 1:    number = signedType ? as(int8_t) : as(uint8_t); break;
      case 2:    number = signedType ? as(int16_t) : as(uint16_t); break;
      case 3:    number = as(int32_t) % 16777216; break;
      case 4:    number = as(int32_t); break;
      default:   break;
    }
 
    #undef as

    if ((stepWidth != 4) || (signedType != 0))
      DFL_serializeI32(streamFunc,number,base,pad); 
    else
      DFL_serializeU32(streamFunc,number,base,pad); 

    streamFunc(' ');

    for (uint8_t i = 0; i < stepWidth; ++i)
    {
      char c = *memory;
      *asciiPos = DFL_charIsPrintable(c) ? c : '?';
      asciiPos++;
      memory++;
    }

    column--;
    steps--;
  }  

  DFL_SET_SUCCESS
  return result;
}

uint32_t DFL_serializeU32(DFL_StreamWriteFunc streamFunc, uint32_t n, uint8_t base, uint8_t zeroPad)
{
  DFL_charsSerialized = 0;

  if (base == 0 || base > DFL_MAX_BASE)
  {
    DFL_SET_ERROR(DFL_ERROR,"U32 serialization illegal base");
    return DFL_charsSerialized;
  }

  if (base == 1)
    zeroPad = 0;
  else
    zeroPad = DFL_MAX(1,zeroPad); // to display 0 if n == 0 

  if (n == 0 && base != 1)
  {
    for (uint8_t i = 0; i < zeroPad; ++i)
      streamFunc('0');

    return DFL_charsSerialized;
  }

  char b[34];
  char *buff = b;

  int8_t digits = 0;

  while (((n != 0) || (digits < zeroPad)) && (digits < 33))
  {
    digits++;

    *buff = DFL_digitSymbol(n % base);

    buff++;

    if (base > 1)
      n /= base;
    else
      n--;
  }

  while (digits > 0)
  {
    digits--;
    buff--;
    streamFunc(*buff);
  }

  DFL_SET_SUCCESS
  return DFL_charsSerialized;
}

uint32_t DFL_serializeI32(DFL_StreamWriteFunc streamFunc, int32_t n, uint8_t base, uint8_t zeroPad)
{
  if (n >= 0)
  {
    return DFL_serializeU32(streamFunc,n,base,zeroPad);
  }
  else
  {
    streamFunc('-');
    return 1 + DFL_serializeU32(streamFunc,-1 * n,base,zeroPad);
  }
}

uint32_t DFL_serializeU64(DFL_StreamWriteFunc streamFunc, uint64_t n, uint8_t base, uint8_t zeroPad)
{ 
  /* Not very elegant function, but we want the basic integer serialization
     version not use int64_t, so we do it this way. */

  DFL_charsSerialized = 0;
 
  if (base == 0 || base > DFL_MAX_BASE)
  {
    DFL_SET_ERROR(DFL_ERROR,"U64 serialization illegal base");
    return 0;
  }

  if ((n == 0) && (zeroPad == 0))
    zeroPad = 1;

  // separate U64 into multiple parts of U32

  uint8_t digitsSplit = DFL_maxDigits(4,base,0) - 1;

  uint64_t divisor = 1;

  for (uint8_t i = 0; i < digitsSplit; ++i)
    divisor *= base;

  uint32_t parts[3];
  uint32_t *currPart = parts;
  uint8_t numParts = 0;

  int8_t newZeroPad = zeroPad;

  do
  {
    *currPart = n % divisor;

    n /= divisor;

    numParts++;
    currPart++;

    if (n > 0)
      newZeroPad -= digitsSplit;

  } while (n > 0);

  newZeroPad = newZeroPad >= 0 ? newZeroPad : 0;

  uint64_t tmp = DFL_charsSerialized;

  while (numParts > 0)
  {
    currPart--;
    tmp += DFL_serializeU32(streamFunc,*currPart,base,newZeroPad);
    newZeroPad = digitsSplit;
    numParts--;
  }

  DFL_charsSerialized = tmp;

  DFL_SET_SUCCESS
  return DFL_charsSerialized;
}

uint32_t DFL_serializeI64(DFL_StreamWriteFunc streamFunc, int64_t n, uint8_t base, uint8_t zeroPad)
{
  if (n >= 0)
  {
    return DFL_serializeU64(streamFunc,n,base,zeroPad);
  }
  else
  {
    streamFunc('-');
    return 1 + DFL_serializeU64(streamFunc,-1 * n,base,zeroPad);
  }
}

int32_t DFL_deserializeI32(DFL_StreamReadFunc streamFunc, uint8_t base)
{
  if (base == 0 || base > DFL_MAX_BASE)
  {
    DFL_SET_ERROR(DFL_ERROR,"I32 deserialization illegal base");
    return 0;
  }

  char c;

  int32_t result = 0;
  DFL_Bool negative = DFL_FALSE;

  do
  {
    c = streamFunc();
  } while (DFL_charIsWhite(c));

  if (c == '-')
  {
    negative = DFL_TRUE;
    c = streamFunc();
  }

  int8_t digit;

  DFL_Bool success = DFL_FALSE;

  while (1)
  {
    digit = DFL_symbolDigit(c);

    if (digit < 0 || digit >= base) // not a valid digit?
      break;

    success = DFL_TRUE;

    result = result * base + digit;  

    c = streamFunc();
  }

  if (!success)
  {
    DFL_SET_ERROR(DFL_ERROR,"I32 deserialization no digits");
    return 0;
  }

  return negative ? (-1 * result) : result;
}

uint8_t _DFL_romanDigit(
  uint8_t remainder, char c1, char c2, char c3, char *buff)
{
  uint8_t chars = 0;  

  uint8_t a = (remainder != 4 && remainder != 9) ? (remainder % 5) : 1;

  char before = remainder == 4 ? c2 : (remainder == 9 ? c3 : 0);
  char after = (remainder > 4 && remainder != 9) ? c2 : 0; 
  
  if (before)
  {
    buff[0] = before;
    chars++;
  }

  for (uint8_t i = 0; i < a; ++i)
  {
    buff[chars] = c1;
    chars++;
  }

  if (after)
  {
    buff[chars] = after;
    chars++;
  }

  return chars;
}

void DFL_serializeRoman(DFL_StreamWriteFunc streamFunc, uint16_t n)
{
  if (n < 1 || n > DFL_ROMAN_MAX)
  {
    DFL_SET_ERROR(DFL_ERROR,"unserializable roman numeral");
    return;
  }

  char buff[16];

  int8_t pos = 0;

  pos = _DFL_romanDigit(n % 10,'I','V','X',buff);

  n /= 10;

  if (n != 0)
  {
    pos += _DFL_romanDigit(n % 10,'X','L','C',buff + pos);

    n /= 10;
 
    if (n != 0)
    {
      pos += _DFL_romanDigit(n % 10,'C','D','M',buff + pos);

      n /= 10;
     
      if (n != 0)
        pos += _DFL_romanDigit(n % 10,'M',' ',' ',buff + pos);
    }
  }
  
  while (pos > 0)
  {
    pos--;
    streamFunc(buff[pos]);
  }

  DFL_SET_SUCCESS
}

uint16_t DFL_romanDigitValue(char digit)
{
  switch (digit)
  {
    case 'I': 
    case 'i': return 1;    break;

    case 'V': 
    case 'v': return 5;    break;

    case 'X': 
    case 'x': return 10;   break;

    case 'L': 
    case 'l': return 50;   break;

    case 'C': 
    case 'c': return 100;  break;

    case 'D': 
    case 'd': return 500;  break;

    case 'M': 
    case 'm': return 1000; break;

    default:  return 0;    break;
  }
}

uint16_t DFL_deserializeRoman(DFL_StreamReadFunc streamFunc)
{
  char c = 0;

  do
  {
    c = streamFunc();
  } while (DFL_charIsWhite(c));

  uint16_t result = 0;
  uint16_t previous = 0;
  uint16_t tmpSum = 0;

  while (1)
  {
    uint16_t val = DFL_romanDigitValue(c);

    if (val == 0)
    {
      result += tmpSum;
      break;
    }

    if (val != previous)
    {
      result += (val < previous) ? tmpSum : (-1 * tmpSum);
      tmpSum = val;
    }
    else
      tmpSum += val;

    previous = val;

    c = streamFunc();
  }
  
  return result;
}

uint32_t DFL_toBase(uint16_t number, uint8_t base)
{
  if (base > 10 || base < 2)
  {
    DFL_SET_ERROR(DFL_ERROR,"base conversion illegal base");
    return 0;
  }

  uint32_t result = 0;

  uint32_t tens = 1;

  while (number != 0)
  {
    result += tens * (number % base);
    tens *= 10;
    number /= base;
  }  

  DFL_SET_SUCCESS

  return result;
}

void DFL_serializeUtf8Char(DFL_StreamWriteFunc streamFunc, uint32_t wChar)
{
  if (wChar <= 0x7f)
  {
    streamFunc(wChar);
  }
  else if (wChar <= 0x7ff)
  {
    streamFunc(((wChar >> 6) & DFL_00011111) | DFL_11000000);
    streamFunc((wChar & DFL_00111111) | DFL_10000000);
  }
  else if (wChar <= 0xffff)
  {
    streamFunc(((wChar >> 12) & DFL_00001111) | DFL_11100000);
    streamFunc(((wChar >> 6) & DFL_00111111) | DFL_10000000);
    streamFunc((wChar & DFL_00111111) | DFL_10000000);
  }
  else
  {
    streamFunc(((wChar >> 18) & DFL_00000111) | DFL_11110000);
    streamFunc(((wChar >> 12) & DFL_00111111) | DFL_10000000);
    streamFunc(((wChar >> 6) & DFL_00111111) | DFL_10000000);
    streamFunc((wChar & DFL_00111111) | DFL_10000000);
  }
}
  
uint32_t DFL_deserializeUtf8Char(DFL_StreamReadFunc streamFunc)
{
  uint32_t result = 0;

  char c = streamFunc();

  if ((c & DFL_10000000) == 0)
    return c;
  else
  {
    uint8_t extraBytes;

    if ((c & DFL_11100000) == DFL_11000000)
    {
      extraBytes = 1;
      result = c & DFL_00011111;
    }
    else if ((c & DFL_11110000) == DFL_11100000)
    {
      extraBytes = 2;
      result = c & DFL_00001111;
    }
    else
    {
      extraBytes = 3;
      result = c & DFL_00000111;
    }
    
    for (uint8_t i = 0; i < extraBytes; ++i)
    {
      c = streamFunc();

      result = (result * 64) + (c & DFL_00111111);
    }
  }

  return result;
}

char *DFL_streamBitArrayToStr(char *dst, uint8_t *array, uint32_t length)
{
  DFL_strStreamOut = dst;
  DFL_serializeBitArray(DFL_streamToStr,array,length);
  return DFL_strStreamOut;
}
 
const char *DFL_streamUtf8CharFromStr(const char *stream, uint32_t *result)
{
  DFL_strStreamIn = stream;
  *result = DFL_deserializeUtf8Char(DFL_streamFromStr);
  return DFL_strStreamIn;
}

char *DFL_streamStrToStr(char *dst, const char *s)
{
  DFL_strStreamOut = dst;
  DFL_serializeStr(DFL_streamToStr,s);
  return DFL_strStreamOut;
}
  
char *DFL_streamEndToStr(char *dst)
{
  DFL_strStreamOut = dst;
  *DFL_strStreamOut = 0;
  return DFL_strStreamOut;
}

char *DFL_streamI32ToStr(char *dst, int32_t n, uint8_t base, uint8_t zeroPad)
{
  DFL_strStreamOut = dst;
  DFL_serializeI32(DFL_streamToStr,n,base,zeroPad);
  return DFL_strStreamOut;
}
    
char *DFL_streamU32ToStr(char *dst, uint32_t n, uint8_t base, uint8_t zeroPad)
{
  DFL_strStreamOut = dst;
  DFL_serializeU32(DFL_streamToStr,n,base,zeroPad);
  return DFL_strStreamOut;
}
  
void DFL_streamFToOut(float f, uint8_t base, uint8_t maxDecimals)
{
  DFL_serializeF(DFL_streamToOut,f,base,maxDecimals);
}
    
float DFL_streamFFromIn(uint8_t base)
{
  return DFL_deserializeF(DFL_streamFromIn,base);
}
    
const char *DFL_streamFFromStr(const char *stream, uint8_t base, float *result)
{
  DFL_strStreamIn = stream;
  *result = DFL_deserializeF(DFL_streamFromStr,base);
  return DFL_strStreamIn;
}

char *DFL_streamFToStr(char *dst, float f, uint8_t base, uint8_t maxDecimals)
{
  DFL_strStreamOut = dst;
  DFL_serializeF(DFL_streamToStr,f,base,maxDecimals);
  return DFL_strStreamOut;
}

uint16_t DFL_streamRomanFromIn(void)
{
  return DFL_deserializeRoman(DFL_streamFromIn);
}

void DFL_streamStrToOut(char *s)
{
  DFL_serializeStr(DFL_streamToOut,s);
}
 
void DFL_streamStrToFile(char *s)
{
  DFL_serializeStr(DFL_streamToFile,s);
}

void DFL_streamUtf8CharToFile(uint32_t wChar)
{
  DFL_serializeUtf8Char(DFL_streamToFile,wChar);
}

void DFL_streamUtf8CharToOut(uint32_t wChar)
{
  DFL_serializeUtf8Char(DFL_streamToOut,wChar);
}

void DFL_streamI32ToOut(int32_t n, uint8_t base, uint8_t zeroPad)
{
  DFL_serializeI32(DFL_streamToOut,n,base,zeroPad);
}

void DFL_streamI32ToFile(int32_t n, uint8_t base, uint8_t zeroPad)
{
  DFL_serializeI32(DFL_streamToFile,n,base,zeroPad);
}
    
void DFL_streamU32ToOut(uint32_t n, uint8_t base, uint8_t zeroPad)
{
  DFL_serializeU32(DFL_streamToOut,n,base,zeroPad);
}

void DFL_streamRomanToOut(uint16_t n)
{
  DFL_serializeRoman(DFL_streamToOut,n);
}

void DFL_streamU64ToOut(uint64_t n, uint8_t base, uint8_t zeroPad)
{
  DFL_serializeU64(DFL_streamToOut,n,base,zeroPad);
}

char *DFL_streamU64ToStr(char *dst, uint64_t n, uint8_t base, uint8_t zeroPad)
{
  DFL_strStreamOut = dst;
  DFL_serializeU64(DFL_streamToStr,n,base,zeroPad);
  return DFL_strStreamOut; 
}

void DFL_streamI64ToOut(int64_t n, uint8_t base, uint8_t zeroPad)
{
  DFL_serializeI64(DFL_streamToOut,n,base,zeroPad);
}

char *DFL_streamI64ToStr(char *dst, int64_t n, uint8_t base, uint8_t zeroPad)
{
  DFL_strStreamOut = dst;
  DFL_serializeI64(DFL_streamToStr,n,base,zeroPad);
  return DFL_strStreamOut; 
}
  
char *DFL_streamRomanToStr(char *dst, uint16_t n)
{
  DFL_strStreamOut = dst;
  DFL_serializeRoman(DFL_streamToStr,n);
  return DFL_strStreamOut;
}

const char *DFL_streamRomanFromStr(const char *stream, uint16_t *result)
{
  DFL_strStreamIn = stream;
  *result = DFL_deserializeRoman(DFL_streamFromStr);
  return DFL_strStreamIn;
}

int32_t DFL_streamI32FromIn(uint8_t base)
{
  return DFL_deserializeI32(DFL_streamFromIn,base);
}

const char *DFL_streamI32FromStr(const char *stream, uint8_t base,
  int32_t *result)
{
  DFL_strStreamIn = stream;

  *result = DFL_deserializeI32(DFL_streamFromStr,base);

  return DFL_strStreamIn;
}

uint32_t DFL_brainfuckRun(const char *program, char *tape, uint16_t tapeSize,
    DFL_StreamWriteFunc fOut, DFL_StreamReadFunc fIn)
{
  DFL_LOG("Executing brainfuck.");

  uint16_t programPos = 0, tapePos = 0;
  uint32_t steps = 0;

  while (
    DFL_brainfuckStep(
      program,
      tape,
      tapeSize,
      &programPos,
      &tapePos,
      fOut,
      fIn
    ))
    steps++;

  return steps;
}

DFL_Bool DFL_brainfuckStep(const char *program, char *tape, uint16_t tapeSize, 
  uint16_t *programPos, uint16_t *tapePos, DFL_StreamWriteFunc fOut,
  DFL_StreamReadFunc fIn)
  {
    #define moveAfterEndBracket(dir,b1,b2)\
      {\
        uint16_t bracketCount = 1;\
        DFL_Bool goOn = DFL_TRUE;\
        while (goOn)\
        {\
          *programPos += dir;\
          switch (program[*programPos])\
          {\
            case 0: return DFL_FALSE; break;\
            case b1: bracketCount++; break;\
            case b2:\
              bracketCount--;\
              if (bracketCount == 0)\
                goOn = DFL_FALSE;\
              break;\
              default: break;\
          }\
        }\
      }
    
    DFL_Bool end = DFL_FALSE;

    while (!end)
    {
      switch (program[*programPos])
      {
        case 0: return DFL_FALSE; break;

        case '>':
          (*tapePos)++;

          if (*tapePos >= tapeSize)
            *tapePos = 0;  
          
          end = DFL_TRUE;
          break;

        case '<':
          *tapePos = *tapePos > 0 ? (*tapePos - 1) : (tapeSize - 1);
          end = DFL_TRUE;
          break;

        case '+': tape[*tapePos]++;       end = DFL_TRUE; break;
        case '-': tape[*tapePos]--;       end = DFL_TRUE; break;

        case '.': 
          if (fOut != 0)
            fOut(tape[*tapePos]);         end = DFL_TRUE; break;

        case ',':
          if (fIn != 0)
            tape[*tapePos] = fIn();       end = DFL_TRUE; break;

        case '[':
         if (tape[*tapePos] == 0)
           moveAfterEndBracket(1,'[',']')

         end = DFL_TRUE;
         break;

        case ']':
         if (tape[*tapePos] != 0)
           moveAfterEndBracket(-1,']','[')
         
         end = DFL_TRUE;
         break;

        default: break;
      }

      (*programPos)++;
    }

    #undef moveAfterEndBracket

    return DFL_TRUE;
  }

char DFL_charToUpper(char c)
{
  return (c >= 'a' && c <= 'z') ? (c - 'a' + 'A') : c;
}

char DFL_charToLower(char c)
{
  return (c >= 'A' && c <= 'Z') ? (c - 'A' + 'a') : c;
}

char *DFL_stringToUpper(char *s)
{
  char *c = s;

  while (*c != 0)
  {
    *c = DFL_charToUpper(*c);
    c++;
  }

  return s;
}

char *DFL_stringToLower(char *s)
{
  char *c = s;

  while (*c != 0)
  {
    *c = DFL_charToLower(*c);
    c++;
  }

  return s;
}

//-----------

void DFL_streamToStr(char c)
{
  if ((DFL_strStreamOut != 0) && (*DFL_strStreamOut != 0))
  {
    *DFL_strStreamOut = c;
    DFL_strStreamOut++;
    DFL_charsSerialized++;
  }
}

char DFL_streamFromStr(void)
{
  const char c = *DFL_strStreamIn;

  if (c != 0)
    DFL_strStreamIn++;

  return c;
}
    
void DFL_streamToNull(char c)
{
  DFL_charsSerialized++;
}

void DFL_streamToOut(char c)
{
  DFL_charsSerialized++;
  DFL_PUT_CHAR(c);
}

char DFL_streamFromIn(void)
{
  // { I get compilation error about DFL_GET_CHAR 
  // so I'll just return 0 for now ~Dolphinana }
	/*
#ifdef DFL_GET_CHAR
  return DFL_GET_CHAR;
#else
*/
  return 0;
//#endif
}
    
void DFL_streamToFile(char c)
{
  DFL_charsSerialized++;
  DFL_FILE_WRITE(c)
}

char DFL_streamFromFile(void)
{
  char c;
  DFL_FILE_READ(c)
  return c;
}

char DFL_unicodeToAscii(uint32_t wChar)
{
  if (wChar <= 0x7f)
    return wChar;

  // TODO: first check homoglyphs

  // TODO: chinese/japanese characters to '#' ?

  else
    return '?';
}

uint32_t DFL_getUnicodeHomoglyph(uint32_t wChar)
{
  if (wChar <= 127)
    return wChar;

  return 0;

  // TODO
}

DFL_Bool DFL_floatsEqual(float a, float b, float tolerance)
{
  return (b < (a + tolerance)) && (b > (a - tolerance));
}

int32_t DFL_minI32(int32_t a, int32_t b)       { return DFL_MIN(a,b); }
int32_t DFL_minF(float a, float b)             { return DFL_MIN(a,b); }

int32_t DFL_maxI32(int32_t a, int32_t b)       { return DFL_MAX(a,b); }
int32_t DFL_maxF(float a, float b)             { return DFL_MAX(a,b); }

static inline int8_t  DFL_signI8(int8_t x)     { return DFL_SIGN(x); }
static inline int32_t DFL_signI32(int32_t x)   { return DFL_SIGN(x); }
static inline int32_t DFL_signF(float x)       { return DFL_SIGN(x); }

int32_t DFL_nonzeroI32(int32_t x)              { return DFL_NONZERO(x); }
int32_t DFL_nonzeroF(float x)                  { return DFL_NONZERO(x); }

int32_t DFL_wrapI32(int32_t x, uint32_t limit) { return DFL_WRAP(x,(int32_t) limit); }
int32_t DFL_wrapF(float x, float limit)        { return 0; /* TODO */ }

float DFL_modF(float f, float m)               { return DFL_MOD_F(f,m); }

//-----------

DFL_Bool DFL_chessPieceIsWhite(char piece)
{
  return piece >= 'a';
}
  
DFL_Bool DFL_chessSquareIsWhite(uint8_t square)
{
  return (square % 2) == ((square / 8) % 2);
}

void DFL_chessGetPieceMoves(const DFL_ChessState state, uint8_t pieceSquare, DFL_ChessMoveSet result)
{
  uint8_t moves = 0;

  char piece = state[pieceSquare];

  DFL_Bool isWhite = DFL_chessPieceIsWhite(piece);

  int8_t horizontalPosition = pieceSquare % 8;

  switch(piece)
  {
    case 'n': // knight
    case 'N':
      {
        int8_t offsets[4] = {6,10,15,17};
        int8_t columnsMinus[4] = {2,-2,1,-1};
        int8_t columnsPlus[4] = {-2,2,-1,1};
        int8_t *off, *col;

        #define checkOffsets(op,comp,limit,dir)\
          off = offsets;\
          col = columns ## dir;\
          for (uint8_t i = 0; i < 4; ++i, ++off, ++col)\
          {\
            int8_t square = pieceSquare op (*off);\
            if (square comp limit) /* out of board? */\
              break;\
            int8_t horizontalCheck = horizontalPosition + (*col);\
            if (horizontalCheck < 0 || horizontalPosition >= 8)\
              continue;\
            char squareC = state[square];\
            if ((squareC == '.') || (DFL_chessPieceIsWhite(squareC) != isWhite))\
            {\
              result[moves] = square;\
              moves++;\
            }\
          }

        checkOffsets(-,<,0,Minus)
        checkOffsets(+,>=,64,Plus)

        #undef checkOffsets
      } 
      break;   

    default:
      break;
  }

  result[moves] = 255;
} 

uint8_t *DFL_memoryCopy(const uint8_t *src, uint16_t size, uint8_t *dst)
{
  if ((src == DFL_NULL) || (dst == DFL_NULL))
  {
    DFL_SET_ERROR(DFL_ERROR,"memory copy with NULL");
    return dst;
  }

  const uint8_t *s = src;
  uint8_t *d = d;

  for (int32_t i = 0; i < size; ++i)
  {
    *d = *s;
    s++;
    d++;
  }

  DFL_SET_SUCCESS

  return dst;
}

uint8_t *DFL_memorySet(uint8_t *memory, uint16_t size, uint8_t value)
{
  if (memory == DFL_NULL)
  {
    DFL_SET_ERROR(DFL_ERROR,"memory set with NULL");
    return memory;
  }

  uint8_t *m = memory;

  for (int32_t i = 0; i < size; ++i)
  {
    *m = value;
    m++;
  }

  DFL_SET_SUCCESS

  return memory;
}

DFL_Bool DFL_memoryCompare(const uint8_t *memory, const uint8_t *memory2, uint16_t size)
{
  if ((memory == DFL_NULL) || (memory2 == DFL_NULL))
  {
    DFL_SET_ERROR(DFL_ERROR,"memory compare with NULL");
    return DFL_FALSE;
  }
 
  const uint8_t *m1 = memory;
  const uint8_t *m2 = memory;

  for (int32_t i = 0; i < size; ++i)
  {
    if (*m1 != *m2)
      return DFL_FALSE;

    m1++;
    m2++;
  }
  
  DFL_SET_SUCCESS
  return DFL_TRUE;
}
  
uint8_t DFL_numberDigits(int64_t number, uint16_t base)
{
  if (base <= 1)
  {
    DFL_SET_ERROR(DFL_ERROR,"no. of digits illegal base");
    return 0;
  }

  uint8_t result = 0;

  while (number > 0)
  {
    number /= base;
    result++;
  }

  DFL_SET_SUCCESS
  return result;
}

uint8_t DFL_maxDigits(uint8_t byteWidth, uint16_t base, DFL_Bool signedType)
{
  if (base <= 1)
  {
    DFL_SET_ERROR(DFL_ERROR,"max digits with illegal base");
    return 0;
  }

  uint64_t maxNumber = 1;

  if ((byteWidth != 8) || signedType)
  {
    while (byteWidth > 0)
    {
      maxNumber *= 256;
      byteWidth--;
    }
  
    maxNumber--;
  }
  else
  {
    DFL_SET_ERROR(DFL_ERROR,"max digits can't take U64");
    return 0;
  }
 
  DFL_SET_SUCCESS 
  return DFL_numberDigits(maxNumber,base);
}

/**
  Helper for argument parsing functions.
*/
int16_t _DFL_CLIArgFind(char p, uint8_t argc, const char **argv)
{
  if (p >= 32) // "-cval" format
  {
    for (uint16_t i = 1; i < argc; ++i)
      if ((argv[i][0] == '-') && (argv[i][1] == p))
        return i;
  }
  else
  {
    for (uint16_t i = 1; i < argc; ++i)
      if (argv[i][0] != '-')
      {
        if (p == 0)
          return i;

        p--;
      }
  }
  
  return -1;
}
 
DFL_Bool DFL_CLIArgPresent(char p, uint8_t argc, const char **argv)
{
  return _DFL_CLIArgFind(p,argc,argv) >= 0;
}

const char *DFL_CLIArgGetStr(
  char p, const char *defaultVal, uint8_t argc, const char **argv)
{
  int16_t pos;

  if ((pos = _DFL_CLIArgFind(p,argc,argv)) < 0) return defaultVal;

  return argv[pos] + (p >= 32 ? 2 : 0);
}
  
uint8_t DFL_CLIArgGet0to9(
  char p, uint8_t defaultVal, uint8_t argc, const char **argv)
{
  int16_t pos;

  if ((pos = _DFL_CLIArgFind(p,argc,argv)) < 0) return defaultVal;

  uint8_t plus = p >= 32 ? 2 : 0;

  if (argv[pos][plus + 1] != 0)
    return defaultVal;
  
  char d = argv[pos][plus];

  if (!DFL_charIsDigit(d))
    return defaultVal;

  return d - '0';
}

int32_t DFL_CLIArgGetI32(
  char p, int32_t defaultVal, uint8_t argc, const char **argv)
{
  int16_t pos;

  if ((pos = _DFL_CLIArgFind(p,argc,argv)) < 0) return defaultVal;

  int32_t result;

  DFL_streamI32FromStr(argv[pos] + (p >= 32 ? 2 : 0),10,&result);

  if (DFL_error)
    return defaultVal;

  DFL_SET_SUCCESS
  return result;
}

uint8_t DFL_bitArrayGet(uint8_t *array, uint32_t index)
{
  return (array[index / 8] >> (index % 8)) & 0x01; 
}

uint8_t DFL_bitArraySet(uint8_t *array, uint32_t index, uint8_t value)
{
  if (value != 0)
    array[index / 8] |= (0x01) << (index % 8);
  else
    array[index / 8] &= ~((0x01) << (index % 8));
} 

#endif // guard
