// @game.h
//
// codename: shoot-em-dolphi
// by Dolphinana 2022
// waived under: CC0
//
// Shoot Em Dolphi


#define _SED
#define _SHOOT_EM_DOLPHI
#define _UWU


/* drummylib stuff */
#define SED_MIN(a,b) ((a) < (b) ? (a) : (b))
#define SED_MAX(a,b) ((a) < (b) ? (b) : (a))
#define SED_MIN_CLAMP_MAX(min,x,max) ((min) < (x))

/*
_SAF_CONST int8_t _SED_cosTable[64] =
{
  127,127,127,127,127,127,126,126,125,124,124,123,122,121,120,119,118,117,115,
  114,112,111,109,108,106,104,102,100,98,96,94,92,90,88,85,83,81,78,76,73,71,68,
  65,63,60,57,54,51,48,46,43,40,37,34,31,28,24,21,18,15,12,9,6,3
};
*/

/* the end of drummylib stuff */


#if defined(_WIN32) || defined(WIN32) || defined(__WIN32__) || defined(__NT__) || defined(__APPLE__)
  #define SED_OS_IS_MALWARE 1
#endif


#ifndef SED_FPS
  #define SED_FPS 50
#endif

#ifndef SED_LOG
  #define SED_LOG(str) {} // 
#endif

#define SED_SETTINGS_DEBUG 1

#ifdef SED_SETTINGS_LQ
  #define SED_SCREEN_WIDTH 64
  #define SED_SCREEN_HEIGHT 64
#else
  #define SED_SCREEN_WIDTH 64 * 8
  #define SED_SCREEN_HEIGHT 64 * 8
#endif


#define SED_SCREEN_WIDTH_HALF SED_SCREEN_WIDTH / 2
#define SED_SCREEN_HEIGHT_HALF SED_SCREEN_HEIGHT / 2

#define SED_MODE_MENU 0
#define SED_MODE_ONFLY 1

#define SED_UNIT_PER_PIXEL 8
#define SED_PIXEL_PER_GRID 8
#define SED_UNIT_PER_GRID SED_UNIT_PER_PIXEL * SED_PIXEL_PER_GRID

// Level constants
#define SED_LVL_ENEMY_ALLOC 128
#define SED_LVL_LEFT_BORDER SED_PIXEL_TO_UNIT(32)
#define SED_LVL_RIGHT_BORDER SED_LVL_LEFT_BORDER + SED_PIXEL_TO_UNIT(SED_SCREEN_WIDTH)
#define SED_LVL_W 8

// Setting the value to SED_PIXEL_TO_UNIT(16) creates a weird lag at the end
#define SED_LVL_END_POS SED_PIXEL_TO_UNIT(64)

// Player constants
#define SED_PLAYER_INIT_X \
  SED_PIXEL_TO_UNIT(SED_SCREEN_WIDTH_HALF - 4.5) + SED_LVL_LEFT_BORDER 
  
#define SED_PLAYER_MOVE_SPEED SED_PIXEL_TO_UNIT(60) / SED_FPS
//#define SED_PLAYER_MOVE_SPEED SED_PIXEL_TO_UNIT(2) 
#define SED_PLAYER_STATE_DOWN 0
#define SED_PLAYER_STATE_NORMAL 1
#define SED_PLAYER_FORWARD_SLOW SED_PIXEL_TO_UNIT(60) / SED_FPS
#define SED_PLAYER_FORWARD_FAST SED_PIXEL_TO_UNIT(120) / SED_FPS

// Enemy constants
#define SED_ENEMY_STATE_DOWN 0
#define SED_ENEMY_STATE_ALIVE 1
// Warning: not final enemy names
#define SED_ENEMY_TYPE_DUMMY 0
#define SED_ENEMY_TYPE_SPIKEY 1
#define SED_ENEMY_TYPE_REDEYEDSPIKEY 2
#define SED_ENEMY_TYPE_HOMAN 3
#define SED_ENEMY_TYPE_FORRY 4

#define SED_ENEMY_DUMMY_W SED_PIXEL_TO_UNIT(9)
#define SED_ENEMY_DUMMY_H SED_PIXEL_TO_UNIT(9)


// Bullet constants
#define SED_BULLET_COUNT 5
#define SED_BULLET_STATE_NONE 0
#define SED_BULLET_STATE_OUT 1
// Bullet's width and height
#define SED_BULLET_W SED_PIXEL_TO_UNIT(3)
#define SED_BULLET_H SED_PIXEL_TO_UNIT(3)
#define SED_PLAYER_BULLET_SPEED SED_PIXEL_TO_UNIT(256) / SED_FPS
// The speed has to be faster than player's forward speed
//#define SED_PLAYER_BULLET_SPEED SED_PIXEL_TO_UNIT(130) / SED_FPS


// Controller constants
#define SED_BUTTON_COUNT 5
#define SED_BUTTON_LEFT 0
#define SED_BUTTON_RIGHT 1
#define SED_BUTTON_UP 2
#define SED_BUTTON_DOWN 3
#define SED_BUTTON_SHOOT 4

// Debug constants
#define SED_DEBUG_PIXEL_GAP 18// THINK ABOUT THIS!


// Macro "functions"

#define SED_PIXEL_TO_UNIT(n) ((n) * (SED_UNIT_PER_PIXEL))
#define SED_UNIT_TO_PIXEL(n) ((n) / (SED_UNIT_PER_PIXEL))
#define SED_UNIT_TO_GRID(n)  ((n) / (SED_UNIT_PER_GRID)) 
#define SED_GRID_TO_UNIT(g)  ((g) * (SED_UNIT_PER_GRID))
#define SED_GET_PLAYER_CENTER_X

#define MEAN3(a,b,c) (((a) + (b) + (c)) / 3)
// Self note:
// You need many brackets when writing "macro functions",
// otherwise, the returned value could be wrong if you 
// put in (5 + 1) into HALF, if it does not use many brackets, 
// it would return 5 and the processed code would be (5 + 1 / 2)
#define HALF(x) ((x) / 2)


//#include "drummylib.h"

#include "images.h"
#include "level.h" // should be levels.h


uint32_t _SED_frame = 0;

/* this is a vector struct made specifically for 
   this game. */
typedef struct
{
  uint16_t x;
  uint32_t y;
} SED_Vec2;


struct
{
  uint8_t state;      // <<< Current game state
  uint8_t buttonPressed[SED_BUTTON_COUNT];
  uint32_t oceanPosition[2];   /* <<< This should be replaced with something 
              more efficient. */
} SED_game;


uint16_t old_ly;

// this game will only have one camera, so this struct is not typedef'd
struct
{
  uint32_t posY;
  int8_t offset[2];
} SED_camera;



// player
struct
{
  uint8_t state;
  SED_Vec2 pos;
  uint8_t size[2];
} SED_player;

int8_t SED_shipForwardSpeed;


// bullet
typedef struct
{
  uint8_t state;
  SED_Vec2 pos;
} SED_BulletDef;

SED_BulletDef SED_bullet[SED_BULLET_COUNT];

// enemy
typedef struct
{
  uint8_t state;
  uint8_t type;
  SED_Vec2 pos;
} SED_EnemyDef;


struct
{
  uint8_t levelNumber;
  uint8_t* levelPointer;
  //
  // in grid
  uint8_t levelW;
  uint16_t levelH;
  uint8_t* background;
  SED_EnemyDef enemy[SED_LVL_ENEMY_ALLOC];
  uint8_t curEnemyCount;


} SED_curLevel;

uint32_t spinny[16][2];

//uint8_t pix_posY = (~(SED_camera.posY % 10) + 10);
uint8_t pix_posY = 0;



//======= functions below will be implemented in frontend file =======//
void SED_processButtonPress();
void SED_drawPixel(uint16_t x, uint16_t y, uint8_t color);

void SED_playShootSound();



//======= functions below will be defined and implemented =======//
//
void SED_doButtonPress();
uint16_t SED_getObjectDrawPosX(uint32_t pos);
uint16_t SED_getObjectDrawPosY(uint32_t pos);
//
// Player related functions
void     SED_playerDoEnemyCollisionAll();
void     SED_playerClampBorder();
SED_Vec2 SED_playerGetCenterPos();
uint32_t SED_playerGetCenterPosX();
uint32_t SED_playerGetCenterPosY();
void     SED_playerSetForwardSpeed(uint8_t forwardSpeed);
void     SED_playerBulletForward();
void     SED_playerDoShootPress();
// bullet related function
//
// enemy related function
void SED_enemyUpdate();
// check overlapping, returns 1 if they are overlapping
uint8_t SED_checkOverlapping(uint32_t x0, uint32_t y0, uint16_t w0, uint16_t h0,
                            uint32_t x1, uint32_t y1, uint16_t w1, uint16_t h1);
//=== drawing ===//
// Clearing screen is likely not needed except for solid colored background.
void SED_clearScreen(uint8_t color);
void SED_composeFrame();
void SED_drawRect(int16_t x, int16_t y, uint16_t w, uint16_t h, uint8_t color);
void SED_drawText(const char *text, uint16_t x, uint16_t y, uint8_t color);
void SED_drawImage(uint8_t* image, int16_t x, int16_t y);
void SED_drawPlayer();
void SED_drawAllEnemies();
void SED_drawAllBullets();
void SED_drawOcean();

//======Functions below should be called by the frontend======//
uint8_t SED_init();
uint8_t SED_mainLoop();





/////////////////////////////
// Function implementation //
/////////////////////////////

void SED_doButtonPress()
{

}

uint16_t SED_getObjectDrawPosX(uint32_t pos)
{
  return SED_UNIT_TO_PIXEL(pos - SED_LVL_LEFT_BORDER + SED_camera.offset[0]);
}

uint16_t SED_getObjectDrawPosY(uint32_t pos)
{
  return SED_UNIT_TO_PIXEL(pos - SED_camera.posY + SED_camera.offset[1]);
}

void SED_playerDoEnemyCollisionAll()
{
  for (uint16_t i = 0; i < SED_LVL_ENEMY_ALLOC; ++i)
  {
    SED_EnemyDef* e = &SED_curLevel.enemy[i];
    if (e->state != SED_ENEMY_STATE_DOWN)
    {
      switch (e->type)
      {
      case SED_ENEMY_TYPE_DUMMY:
        if (SED_checkOverlapping(
              SED_playerGetCenterPosX(), SED_playerGetCenterPosY(),
              0, 0, // player's hitbox should be small
              e->pos.x, e->pos.y,
              SED_ENEMY_DUMMY_W, SED_ENEMY_DUMMY_H))
        {
          SED_player.state = SED_PLAYER_STATE_DOWN;
          break;
        }
      }
    }
  }
}

void SED_playerClampBorder()
{
  // is "else if" faster than just "if"'s.
  if (SED_playerGetCenterPosX() < SED_LVL_LEFT_BORDER)
  {
    SED_player.pos.x = SED_LVL_LEFT_BORDER - SED_player.size[0] / 2;
  }
  else if (SED_playerGetCenterPosX() > SED_LVL_RIGHT_BORDER)
  {
    SED_player.pos.x = SED_LVL_RIGHT_BORDER - SED_player.size[0] / 2;
  }
  //
  if (SED_playerGetCenterPosY() < SED_camera.posY)
  {
    SED_player.pos.y = SED_camera.posY - SED_player.size[1] / 2;
  }
  else if (SED_playerGetCenterPosY() > SED_camera.posY + SED_PIXEL_TO_UNIT(SED_SCREEN_HEIGHT))
  {
    SED_player.pos.y = 
      SED_camera.posY + SED_PIXEL_TO_UNIT(SED_SCREEN_HEIGHT) - SED_player.size[1] / 2;
  }
}

SED_Vec2 SED_playerGetCenterPos()
{
  SED_Vec2 p;  
  p.x = SED_player.pos.x + SED_player.size[0] / 2; 
  p.y = SED_player.pos.y + SED_player.size[1] / 2;
  return p;
}

uint32_t SED_playerGetCenterPosX()
{
  return SED_player.pos.x + SED_player.size[0] / 2;
}

uint32_t SED_playerGetCenterPosY()
{
  return SED_player.pos.y + SED_player.size[1] / 2;
}

void SED_playerSetForwardSpeed(uint8_t forwardSpeed)
{
  SED_shipForwardSpeed = forwardSpeed;
}

// bullet related function
void SED_playerBulletForward()
{
  // The scope below will move all the bullets forward if they are out
  for (uint8_t i = 0; i < SED_BULLET_COUNT; ++i)
  {
    if (SED_bullet[i].state == SED_BULLET_STATE_OUT)
    {
                              // Make the bullet's speed vary
      //SED_bullet[i].pos.y -= SED_PIXEL_TO_UNIT(12) - (SAF_frame() % 10) * 2;
      SED_bullet[i].pos.y -= SED_PLAYER_BULLET_SPEED;
    }
  }
}

void SED_playerDoShootPress()
{
  // If shoot button is just pressed and player isn't dead.
  if (SED_game.buttonPressed[SED_BUTTON_SHOOT] == 1 &&
        SED_player.state != SED_PLAYER_STATE_DOWN)
  {
    for (uint8_t i = 0; i < SED_BULLET_COUNT; ++i)
    {
      if (SED_bullet[i].state == SED_BULLET_STATE_NONE)
      {
        SED_bullet[i].state = SED_BULLET_STATE_OUT;
        SED_bullet[i].pos.x = SED_playerGetCenterPosX() - SED_BULLET_W / 2;
        SED_bullet[i].pos.y = SED_player.pos.y - SED_BULLET_H;
        SED_playShootSound();
        break;
      }
    }
  }
}

// enemy related function
void SED_enemyUpdate()
{
  for (uint16_t i = 0; i < SED_LVL_ENEMY_ALLOC; ++i)
  {            
    SED_EnemyDef* e = &SED_curLevel.enemy[i];
    switch (e->type)
    {
    case SED_ENEMY_TYPE_DUMMY:
      if (e->pos.y > SED_camera.posY - SED_ENEMY_DUMMY_H && 
          e->state & SED_ENEMY_STATE_ALIVE)
        e->pos.y -= 7 * SED_shipForwardSpeed / 8;
      break;
    }
  }
}

// 24 bytes to copy which is 192 bits
uint8_t SED_checkOverlapping(uint32_t x0, uint32_t y0, uint16_t w0, uint16_t h0,
                            uint32_t x1, uint32_t y1, uint16_t w1, uint16_t h1)
{
  // This will check wether two objects are overlapping or not
  return (x0 < x1 + w1 &&
      y0 < y1 + h1 &&
      x0 + w0 > x1 &&
      y0 + h0 > y1);
}

void SED_clearScreen(uint8_t color)
{
  for (int y = 0; y < SED_SCREEN_HEIGHT; ++y)
    for (int x = 0; x < SED_SCREEN_WIDTH; ++x)
      SED_drawPixel(x, y, color);
}

void SED_composeFrame()
{
  SED_clearScreen(0);
  switch (SED_game.state)
  {
  case SED_MODE_ONFLY:
    SED_drawOcean();
    break;
  }

  pix_posY += SED_shipForwardSpeed;
  while (pix_posY > SED_DEBUG_PIXEL_GAP * 8)
     pix_posY -= SED_DEBUG_PIXEL_GAP * 8;

  for (uint8_t i = 0; i < 7; ++i)
  {
    // Optimize this later
    // And this thing is doing as it should, now it just needs some 
    // refactoring to make this code more readable and easier to 
    // handle.
    for (uint8_t j = 0; j < 3; ++j)
    {
      uint8_t dy = (SED_UNIT_TO_PIXEL(pix_posY) + i * SED_DEBUG_PIXEL_GAP);
      if (dy < SED_SCREEN_HEIGHT)
        SED_drawPixel((j * SED_DEBUG_PIXEL_GAP) + 4, dy, 255);
      //SED_drawPixel((((j * SED_DEBUG_PIXEL_GAP) + 4) / 2) + SED_SCREEN_WIDTH_HALF, ((SED_UNIT_TO_PIXEL(pix_posY) + i * SED_DEBUG_PIXEL_GAP) / 2) + SED_SCREEN_HEIGHT_HALF - SED_SCREEN_HEIGHT_HALF, 151);
    }
   //////printf("%d \n", pix_posY);
  }
    
  if (SED_player.state != SED_PLAYER_STATE_DOWN)
    SED_drawPlayer();
  else
  {
    SED_drawText("Game Over", 1, 1, 0x00);
    SED_drawText("Game Over", 0, 0, 0xe0);
  }
  SED_drawAllEnemies();
  SED_drawAllBullets();

  /*
  for (uint8_t i = 0; i < 16; ++i)
  {
    spinny[i][0] = SED_playerGetCenterPosX() + (SAF_cos((SED_player.pos.y + i * 7) / 2)) / 2;
    spinny[i][1] = SED_playerGetCenterPosY() + (SAF_sin((SED_player.pos.y + i * 7 ) / 5)) / 2;

    SED_drawRect(SED_getObjectDrawPosX(spinny[i][0]), SED_getObjectDrawPosY(spinny[i][1]), 1, 1, 255);
  }
  */
}

void SED_drawRect(int16_t x, int16_t y, uint16_t w, uint16_t h, uint8_t color)
{ 
  // There are more efficient ways to implement this... Might optimize this later
  if (x >= 0 && y >= 0 &&
    x + w < SED_SCREEN_WIDTH && y + h < SED_SCREEN_HEIGHT)
  {
    for (int16_t dy = y; dy < y + h; dy++)
    {
      for (int16_t dx = x; dx < x + w; dx++)
      {
        SED_drawPixel(dx, dy, color);
      }
    }
  }
  else
  {
    for (int16_t dy = y; dy < y + h; dy++)
    {
      for (int16_t dx = x; dx < x + w; dx++)
      {
        if (dx >= 0 && dy >= 0 &&
          dx < SED_SCREEN_WIDTH && dy < SED_SCREEN_HEIGHT)
            SED_drawPixel(dx, dy, color);
      }
    }

  }
}

void SED_drawText(const char *text, uint16_t x, uint16_t y, uint8_t color)
{
  #ifdef SED_PLATFORM_SAF
  ////printf("uwu\n");
  SAF_drawText(text, x, y, color, 1);
  #else
  #endif
}

void SED_drawImage(uint8_t* image, int16_t x, int16_t y)
{
  uint8_t* pixel = image;
  uint8_t imageW = *pixel;
  pixel++;
  uint8_t imageH = *pixel;
  //
  for (int16_t dy = y; dy < y + imageH; ++dy)
  {
    for (int16_t dx = x; dx < x + imageW; ++dx)
    {
      pixel++;
      if (dx >= 0 && dx < SED_SCREEN_WIDTH &&
        dy >= 0 && dy < SED_SCREEN_HEIGHT && *pixel != 0x00)
      {
        SED_drawPixel(dx, dy, *pixel);
      }
    }
  }
}




void SED_drawPlayer()
{
  if (SED_player.state != SED_PLAYER_STATE_DOWN)
    SED_drawImage(IMAGE_ship, 
    SED_getObjectDrawPosX(SED_player.pos.x),
    SED_getObjectDrawPosY(SED_player.pos.y)
    );
}


/*
void SED_drawSpikeyRed()
{
for (uint8_t i = 0; i < SED_LVL_ENEMY_ALLOC; ++i)
if (SED_curLevel.enemy[i].state != SED_ENEMY_STATE_DOWN)
SED_drawImage(IMAGE_spikey_red, 
SED_getObjectDrawPosX(SED_curLevel.enemy[i].pos.x),
SED_getObjectDrawPosY(SED_curLevel.enemy[i].pos.y)
);
}
*/

void SED_drawAllEnemies()
{
  for (uint16_t i = 0; i < SED_LVL_ENEMY_ALLOC; ++i)
  {
    SED_EnemyDef* e = &SED_curLevel.enemy[i];
    if (e->state != SED_ENEMY_STATE_DOWN)
    {
      switch (e->type)
      {
      case SED_ENEMY_TYPE_DUMMY:
        SED_drawImage(IMAGE_spikey_red, 
          SED_getObjectDrawPosX(e->pos.x),
          SED_getObjectDrawPosY(e->pos.y)
          );
      }
    }
  }
}

void SED_drawAllBullets()
{
  // Draw bullet
  for (uint8_t i = 0; i < SED_BULLET_COUNT; ++i)
  {
    if (SED_bullet[i].state == SED_BULLET_STATE_OUT)
      SED_drawRect(
        SED_getObjectDrawPosX(SED_bullet[i].pos.x),
        SED_getObjectDrawPosY(SED_bullet[i].pos.y),
        SED_UNIT_TO_PIXEL(SED_BULLET_W), 
        SED_UNIT_TO_PIXEL(SED_BULLET_H), 
        255);
  }
}

void SED_drawOcean()
{
  // four copies of ocean image is needed to make it look seamless
  //SED_drawImage(IMAGE_ocean_background, 0, 
       //SED_game.oceanPosition[1]);
  SED_drawImage(IMAGE_ocean_background, 
  SED_getObjectDrawPosX(SED_game.oceanPosition[0]),
  SED_getObjectDrawPosY(SED_game.oceanPosition[1])
  );
  SED_drawImage(IMAGE_ocean_background, 
  SED_getObjectDrawPosX(SED_game.oceanPosition[0]),
  SED_getObjectDrawPosY(SED_game.oceanPosition[1] + SED_PIXEL_TO_UNIT(128))
  );
  SED_drawImage(IMAGE_ocean_background, 
  SED_getObjectDrawPosX(SED_game.oceanPosition[0]),
  SED_getObjectDrawPosY(SED_game.oceanPosition[1] + SED_PIXEL_TO_UNIT(256))
  );
}


// Call this from frontend file
uint8_t SED_init()
{
  SED_playerSetForwardSpeed(SED_PLAYER_FORWARD_FAST);

  SED_game.state = SED_MODE_ONFLY;

  for (uint16_t i = 0; i < SED_LVL_ENEMY_ALLOC; ++i)
    SED_curLevel.enemy[i].state = SED_ENEMY_STATE_DOWN;

  SED_curLevel.curEnemyCount = 0;
  SED_curLevel.levelPointer = SED_levelTest2;

  // Level system will probably need a rewrite.
  // quick level initialization code, refactor/rewrite this later
  /*
  {
    // There is a bug where the game loads multiple levels seamlessly,
    // it can happen when the specified length is wrong.
    uint8_t* lvlP = SED_curLevel.levelPointer;
    //SED_curLevel.levelW = *lvlP;
    SED_curLevel.levelW = 8;
    lvlP++;
    SED_curLevel.levelH = *lvlP * 8;
    lvlP++; // Now at the first tile/enemy/whatever
  
    uint16_t eIndex = 0;
    //int8_t eIndex = SED_LVL_ENEMY_ALLOC - 1;
  
    for (uint16_t ly = 0; ly < SED_curLevel.levelH; ly++)
    {
      for (uint8_t lx = 0; lx < SED_curLevel.levelW; lx++)
      {
  
              //eIndex >= 0)
        uint16_t element = *(lvlP + (ly * SED_curLevel.levelW + lx));
       ////printf("%d\n", element);
        switch (element)
        {
        case 0xff:
          SED_curLevel.enemy[eIndex].state = SED_ENEMY_STATE_ALIVE;
          SED_curLevel.enemy[eIndex].type = SED_ENEMY_TYPE_DUMMY;
          SED_curLevel.enemy[eIndex].pos[0] = SED_GRID_TO_UNIT(lx) + SED_LVL_LEFT_BORDER;
          SED_curLevel.enemy[eIndex].pos[1] = SED_GRID_TO_UNIT(ly);
          //printf("%d ", SED_curLevel.enemy[eIndex].pos[1]);
          eIndex++;
          break;
        
        }
        SED_LOG("yep");
      }
    }
    // 27
    //printf("enemy count: %d \n", eIndex);
  
  }
  */

  SED_curLevel.levelH = SED_curLevel.levelPointer[1] * 8;


  SED_player.state = SED_PLAYER_STATE_NORMAL;
  SED_player.size[0] = SED_PIXEL_TO_UNIT(9),  
  SED_player.size[1] = SED_PIXEL_TO_UNIT(9); 


  /*
  for (uint8_t i = 0; i < SED_LVL_ENEMY_ALLOC; ++i)
  {
    SED_ENEMY_DUMMY_W = SED_PIXEL_TO_UNIT(9);
    SED_ENEMY_DUMMY_H = SED_PIXEL_TO_UNIT(9);
  }
  */

  switch (SED_game.state)
  {
  case SED_MODE_ONFLY:
    SED_player.pos.x = SED_PLAYER_INIT_X;
    //SED_player.pos.y = SED_PIXEL_TO_UNIT(7520); 
    SED_player.pos.y = (SED_curLevel.levelH * 8 * 8) + 1000;

    //SED_LVL_LEFT_BORDER = SED_LVL_LEFT_BORDER;
    SED_camera.posY = (SED_player.pos.y - SED_PIXEL_TO_UNIT(SED_SCREEN_HEIGHT_HALF));

    SED_game.oceanPosition[0] = 0;
    SED_game.oceanPosition[1] = SED_camera.posY - 50;

    /*
    for (uint8_t i = 0; i < SED_LVL_ENEMY_ALLOC; ++i)
    {
      SED_curLevel.enemy[i].pos.x = 3 * SED_PIXEL_TO_UNIT(i);
      SED_curLevel.enemy[i].pos.y = SED_PIXEL_TO_UNIT((7460 - i * 4));
      SED_curLevel.enemy[i].state = 1;
    }
    for (uint8_t i = 0; i < SED_BUTTON_COUNT; ++i)
      SED_game.buttonPressed[i] = 0;
      */

  }
  // return success
  return 1;
}

// Call this from frontend file
uint8_t SED_mainLoop()
{

  if (SED_game.buttonPressed[SED_BUTTON_LEFT])
  {
    SED_player.pos.x -= SED_PLAYER_MOVE_SPEED;
  }
  if (SED_game.buttonPressed[SED_BUTTON_RIGHT])
  {
    SED_player.pos.x += SED_PLAYER_MOVE_SPEED;
  }
  if (SED_game.buttonPressed[SED_BUTTON_UP])
  {
    SED_player.pos.y -= SED_PLAYER_MOVE_SPEED;
  }
  if (SED_game.buttonPressed[SED_BUTTON_DOWN])
  {
    SED_player.pos.y += SED_PLAYER_MOVE_SPEED;
  }

  // If camera reaches the end of level, then stop right there!
  if (SED_camera.posY < SED_LVL_END_POS)
  {
    SED_playerSetForwardSpeed(0);
    ////printf("the end\n");
  }
  SED_player.pos.y -= SED_shipForwardSpeed * (SED_game.state == SED_MODE_ONFLY);
  SED_camera.posY -= SED_shipForwardSpeed * (SED_game.state == SED_MODE_ONFLY);

  // destroy enemies who got out of the border
  for (uint16_t i = 0; i < SED_curLevel.curEnemyCount; i++)
  {
    // Make it so the game will keep count of enemies, so it doesn't have 
    // to check all the dead enemies, it will only check the ones that are alive.
    SED_EnemyDef* e = &SED_curLevel.enemy[i];

    // if camera went past enemy
    if (e->pos.y > SED_camera.posY + SED_PIXEL_TO_UNIT(SED_SCREEN_HEIGHT))
    {
#ifdef SED_SETTINGS_DEBUG
      // if enemy is behind player and is dead
      if (e->state == SED_ENEMY_STATE_DOWN)
      {
        //SED_LOG("Somewhat bug: enemy already out");
      }
#endif
      e->state = SED_ENEMY_STATE_DOWN;
      //printf("enemy out\n");
      // check if index is the same as count of current enemy (minus 1)
      // Might be handy for saving computing power?
      if (i == SED_curLevel.curEnemyCount - 1)
      {
        SED_curLevel.curEnemyCount--;
	SED_LOG("living enemy in the last element memory got removed");
	SED_LOG("curenemycount decremented to " + SED_curLevel.curEnemyCount);
	// I don't know if this scope works.
      }
    }
  }
  
  uint16_t ly = SED_UNIT_TO_GRID(SED_camera.posY);
  for (uint8_t lx = 0; lx < SED_LVL_W; lx++)
  {
    //uint16_t i = SED_curLevel.curEnemyCount;
    uint16_t i;
    for (i = 0; i < SED_LVL_ENEMY_ALLOC; i++)
    {
      if (SED_curLevel.enemy[i].state == SED_ENEMY_STATE_DOWN)
        break;
    }
    uint16_t element = *(SED_curLevel.levelPointer + (ly * SED_LVL_W + lx) + 2);
    //printf("element attribut: %d \n", element);
    //printf("%d \n", i);
    //printf("element location: %d\n", ((ly * SED_curLevel.levelW + lx) + 2));
    //printf("ly: %d\n", ly);
    //printf("w: %d\n", SED_LVL_W);
    // Make sure that current ly isn't same as previous ly
    if (ly != old_ly)
    {
      switch (element)
      {
      case 0xff:
       printf("index = %d\n", i );
        SED_curLevel.enemy[i].state = SED_ENEMY_STATE_ALIVE;
        SED_curLevel.enemy[i].type = SED_ENEMY_TYPE_DUMMY;
        SED_curLevel.enemy[i].pos.x = SED_GRID_TO_UNIT(lx) + SED_LVL_LEFT_BORDER;
        SED_curLevel.enemy[i].pos.y = SED_GRID_TO_UNIT(ly);
        SED_curLevel.curEnemyCount++;
        ////printf("%d ", SED_curLevel.enemy[i].pos.y);
        break;
      
      }
    }
  }
  old_ly = ly;

  uint16_t lastEnemyDeadIndex;
  for (uint16_t i = 0; i < SED_LVL_ENEMY_ALLOC; i++)
  {
    SED_EnemyDef* e = &SED_curLevel.enemy[i];
    if (e->state == SED_ENEMY_STATE_DOWN)
    {
      lastEnemyDeadIndex = i;
#ifdef SED_SETTINGS_DEBUG
      //SED_LOG("Oh shit, the value of i thing is higher than amount of allocated enemies");
      if (i + 1 < SED_LVL_ENEMY_ALLOC)
#endif
      {
        SED_EnemyDef* next = e + 1;
        if (next->state != SED_ENEMY_STATE_DOWN)
        {
          *e = *next;
          next->state = SED_ENEMY_STATE_DOWN;
          next->pos.x = 0;
          next->pos.y = 0;
        }
      }
    }
  }

  // Make sure the player doesn't go outside the screen
  SED_playerClampBorder();

  // Move player's bullets forward
  SED_playerBulletForward();

  // Check if shoot button is pressed. If it is, then shoot!
  SED_playerDoShootPress();

  // Update enemies, generically.
  SED_enemyUpdate();


  if (SED_camera.posY < SED_game.oceanPosition[1])
  {
    SED_game.oceanPosition[1] -= SED_PIXEL_TO_UNIT(128);
  }

  for (uint8_t j = 0; j < SED_BULLET_COUNT; ++j)
  {
    if (SED_bullet[j].state == SED_BULLET_STATE_OUT)
    {
      if (SED_bullet[j].pos.y + SED_BULLET_H < SED_camera.posY)
        SED_bullet[j].state = SED_BULLET_STATE_NONE;

      for (uint16_t i = 0; i < SED_LVL_ENEMY_ALLOC; ++i)
      {
        if (SED_checkOverlapping(SED_bullet[j].pos.x, SED_bullet[j].pos.y, 
              SED_BULLET_W, SED_BULLET_H,
              SED_curLevel.enemy[i].pos.x, SED_curLevel.enemy[i].pos.y, 
              SED_ENEMY_DUMMY_W, SED_ENEMY_DUMMY_H) &&
              //make sure that the bullet doesn't hit dead opponent
              SED_curLevel.enemy[i].state != SED_ENEMY_STATE_DOWN)
        {
          SED_bullet[j].state = SED_BULLET_STATE_NONE;
          SED_curLevel.enemy[i].state = SED_ENEMY_STATE_DOWN;
          break;
        }
      }
    }
  }

  // testing camera offset
  //SED_camera.offset[0] = SAF_sin(SED_player.pos.y >> 3);
  //SED_camera.offset[1] = SAF_cos(SED_player.pos.y >> 3);


  SED_playerDoEnemyCollisionAll();

  //uint64_t u;
//  for (uint64_t i = 0; i < 10000000; ++i)
  //{ u = i;}
  //
  printf("player pos X: %d\n", SED_player.pos.x);
  printf("player pos Y: %d\n", SED_player.pos.y);

   // ============================================ \\
  // ==================== DRAW ==================== \\
   \\ ============================================ //
  
  SED_composeFrame();
  _SED_frame++;
  return 1;
}

