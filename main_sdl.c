//
// @main_sdl.c
//
// codename: shoot-em-dolphi
// by Dolphinana 2022
// waived under: CC0

#include <SDL2/SDL.h>
#include <stdio.h>

SDL_Renderer* sdlRenderer;
SDL_Window* sdlWindow;
SDL_Texture* sdlTexture;
SDL_Event event;

#define SED_LOG(str) puts(str)

#define SED_PLATFORM_SDL
#define SED_SETTINGS_LQ

#define SED_SETTINGS_SCALE 8

#define SED_FPS 60

#include "game.h"

uint8_t sdlScreen[(SED_SCREEN_WIDTH * SED_SCREEN_HEIGHT) * SED_SETTINGS_SCALE];


void SED_processButtonPress()
{
  uint8_t* keyboardState = SDL_GetKeyboardState(NULL);
  SED_game.buttonPressed[SED_BUTTON_LEFT] = keyboardState[SDL_SCANCODE_A];
  SED_game.buttonPressed[SED_BUTTON_RIGHT] = keyboardState[SDL_SCANCODE_D];
  SED_game.buttonPressed[SED_BUTTON_UP] = keyboardState[SDL_SCANCODE_W];
  SED_game.buttonPressed[SED_BUTTON_DOWN] = keyboardState[SDL_SCANCODE_S];



  // TODO: apply this increasing value thing to every other game button
  // TODO: document the code below
  uint8_t shootState = keyboardState[SDL_SCANCODE_J] |
                       keyboardState[SDL_SCANCODE_K] |
                       keyboardState[SDL_SCANCODE_L];
  if (SED_game.buttonPressed[SED_BUTTON_SHOOT] != 0xff)
  {
    SED_game.buttonPressed[SED_BUTTON_SHOOT] += shootState;
  }
  if (shootState == 0)
  {
    SED_game.buttonPressed[SED_BUTTON_SHOOT] = 0;
  }
}

void SED_drawPixel(uint16_t x, uint16_t y, uint8_t color)
{
#ifdef SED_SETTINGS_DEBUG 
  // If the pixel is not drawn in visible area, make a warning.
  if (!(x >= 0 && y >= 0 &&
      x < SED_SCREEN_WIDTH && y < SED_SCREEN_HEIGHT))
    SED_LOG("WARNING: Pixel is drawn outside the screen!");
#endif

  sdlScreen[y * SED_SCREEN_WIDTH + x] = color;
  /*
  // Test code, if pixel drawn isn't black, then draw pixel and render it 
  if (color != 0)
  {
    SDL_UpdateTexture(sdlTexture, NULL, sdlScreen, SED_SCREEN_WIDTH);
    SDL_RenderCopy(sdlRenderer, sdlTexture, NULL, NULL);
    SDL_RenderPresent(sdlRenderer);
  }
  */
}

void SED_playShootSound()
{
}

int main(void)
{
  if (SDL_Init(SDL_INIT_VIDEO) < 0)
  {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Failed to initialize SDL: %s!", SDL_GetError());
    return 3;
  }
  
  if (SDL_CreateWindowAndRenderer(
        SED_SCREEN_WIDTH * SED_SETTINGS_SCALE, 
        SED_SCREEN_HEIGHT * SED_SETTINGS_SCALE, 
        SDL_WINDOW_SHOWN, 
        &sdlWindow, 
        &sdlRenderer))
  {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Failed to initialize window and renderer: %s!", SDL_GetError());
  }

  sdlTexture = SDL_CreateTexture(sdlRenderer,
    SDL_PIXELFORMAT_RGB332, SDL_TEXTUREACCESS_STATIC,
    SED_SCREEN_WIDTH, SED_SCREEN_HEIGHT);

  SED_init();

  while (1)
  {
    uint32_t startTick = SDL_GetTicks(); // Get milliseconds
    //                                  1000 millisecond = 1 second
    while (SDL_GetTicks() - startTick < 1000 / SED_FPS);
    //printf("%d\n", SDL_GetTicks());

    SDL_PollEvent(&event);

    SED_processButtonPress();

    if (event.type == SDL_QUIT)
      break;
    
    SED_mainLoop();

    SDL_UpdateTexture(sdlTexture, NULL, sdlScreen, SED_SCREEN_WIDTH);
    SDL_RenderCopy(sdlRenderer, sdlTexture, NULL, NULL);
    SDL_RenderPresent(sdlRenderer);

  }

  SDL_DestroyRenderer(sdlRenderer);
  SDL_DestroyWindow(sdlWindow);
  SDL_Quit();

  return 0;
}

