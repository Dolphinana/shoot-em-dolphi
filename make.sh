#!/bin/bash


if [ $1 = "sdl" ]; then
	gcc main_sdl.c -o game_sdl -lSDL2   --std=c99 -pg -Os
elif [ $1 = "saf" ]; then
	gcc main_saf.c -o game_saf -lSDL2 -lncurses --std=c99 -g #-Wall
	#gcc main_saf.c -o game_saf -lSDL2 -g --std=c99 #-Wall
else
	echo "Please specify frontends e.g './make.sh sdl'"
fi
