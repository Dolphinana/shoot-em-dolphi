//
// @main_saf.c
//
// codename: shoot-em-dolphi
// by Dolphinana 2022
// waived under: CC0

#define SAF_PROGRAM_NAME "shoot-em-dolphi"
#define SAF_PLATFORM_SDL2
#include "saf.h"

#define SED_FPS SAF_FPS
#define SED_SETTINGS_LQ

#include "game.h"

void SED_processButtonPress()
{
  SED_game.buttonPressed[SED_BUTTON_LEFT] = SAF_buttonPressed(SAF_BUTTON_LEFT);
  SED_game.buttonPressed[SED_BUTTON_RIGHT] = SAF_buttonPressed(SAF_BUTTON_RIGHT);
  SED_game.buttonPressed[SED_BUTTON_UP] = SAF_buttonPressed(SAF_BUTTON_UP);
  SED_game.buttonPressed[SED_BUTTON_DOWN] = SAF_buttonPressed(SAF_BUTTON_DOWN);

  SED_game.buttonPressed[SED_BUTTON_SHOOT] = SAF_buttonPressed(SAF_BUTTON_A)
    | SAF_buttonPressed(SAF_BUTTON_B) | SAF_buttonPressed(SAF_BUTTON_C);
}

void SED_drawPixel(uint16_t x, uint16_t y, uint8_t color)
{
  SAF_drawPixel(x, y, color);
}

void SED_playShootSound()
{
  SAF_playSound(SAF_SOUND_BOOM);
}

void SAF_init(void)
{
  SED_init();
}

uint8_t SAF_loop(void)
{
  SED_processButtonPress();
  return SED_mainLoop();
}

